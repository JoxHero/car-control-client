package com.wissci.carcontrol.common.utils.tool;

import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.ForegroundColorSpan;

/**
 * @author zyp.
 * @data 2020/3/18.
 */
public class SpannableUtils {

    /**
     * 字符串部分文字大小设置
     */
    public static CharSequence changeSizes(String value,int absoluteSize, String... changeSizes) {
        SpannableString str = new SpannableString(value);
        for (int i = 0; i<changeSizes.length;i++){
            String marryValue = changeSizes[i];
            int marryIndex = value.indexOf(marryValue);
            str.setSpan(new AbsoluteSizeSpan(absoluteSize),marryIndex,marryIndex+marryValue.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return str;
    }

    /**
     * 字符串部分文字颜色设置
     */
    public static CharSequence changeColor(String value, String[] changes,int[] colors) {
        SpannableString str = new SpannableString(value);
        for (int i = 0; i<changes.length;i++){
            String marryValue = changes[i];
            int marryIndex = value.indexOf(marryValue);
            str.setSpan(new ForegroundColorSpan(colors[i]),marryIndex,marryIndex+marryValue.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        }
        return str;
    }

    /**
     * 设置部分内容颜色
     */
    public static CharSequence setTextColor(String value, int beginIndex, int endIndex, int color) {
        SpannableString str = new SpannableString(value);
        str.setSpan(new ForegroundColorSpan(color), beginIndex, endIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return str;
    }
}
