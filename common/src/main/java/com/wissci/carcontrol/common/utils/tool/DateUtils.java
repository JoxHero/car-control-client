package com.wissci.carcontrol.common.utils.tool;

import android.util.Log;

import com.wissci.carcontrol.common.Constants;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created zyp on 2016/1/7.
 */
public class DateUtils {

    public static final int SIGN_DYNAMIC = 1;
    public static final int SIGN_TRADE = 2;
    /**
     * 英文简写（默认）如：2010-12-01
     */

    public static String FORMAT_SHORT = "yyyy-MM-dd";
    public static String FORMAT_SHORT2 = "yyyyMMdd";
    public static String FORMAT_SHORT3 = "MM/dd";
    public static String FORMAT_SHORT4 = "yyyy/MM/dd";
    public static String FORMAT_SHORT5 = "MM月dd日";
    public static String FORMAT_SHORT6 = "MM-dd HH:mm";

    /**
     * 英文全称  如：2010-12-01 23:15:06
     */

    public static String FORMAT_LONG = "yyyy-MM-dd HH:mm:ss";
    public static String FORMAT_LONG3 = "yyyy-MM-dd HH:mm";

    public static String FORMAT_LONG2 = "yyyy/MM/dd HH:mm";

    /**
     * 精确到毫秒的完整时间    如：yyyy-MM-dd HH:mm:ss.S
     */

    public static String FORMAT_FULL = "yyyy-MM-dd HH:mm:ss.S";

    /**
     * 中文简写  如：2010年12月01日
     */

    public static String FORMAT_SHORT_CN = "yyyy年MM月dd";
    public static String FORMAT_SHORT_CN2 = "yyyy年MM月";

    /**
     * 中文全称  如：2010年12月01日  23时15分06秒
     */

    public static String FORMAT_LONG_CN = "yyyy年MM月dd日  HH时mm分ss秒";

    /**
     * 精确到毫秒的完整中文时间
     */

    public static String FORMAT_FULL_CN = "yyyy年MM月dd日  HH时mm分ss秒SSS毫秒";

    public static String FORMAT_SHORT_CN_MONTH = "MM月dd日";

    public static String FORMAT_SHORT_HOUR_MIN = "HH:mm";


    /**
     * 获取时间戳
     */
    public static long getTimeString(String time) {
        SimpleDateFormat format = new SimpleDateFormat(FORMAT_SHORT);
        Date date = null;
        try {
            date = format.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();

    }

    /**
     * 获取时间戳
     */
    public static long getStampTimeString(String time, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date == null) {
            return 0;
        }
        return date.getTime();

    }

    /**
     * 获得"yyyy-MM-dd"格式的时间
     *
     * @param timeDate
     * @return
     */
    public static String timeStampToDateChinese(long timeDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_SHORT_CN);
        String sd = sdf.format(new Date(timeDate));
        return sd;
    }

    /**
     * 获得"年-月-日"格式的时间
     *
     * @param timeDate
     * @return
     */
    public static String timeStampToDate(long timeDate) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_SHORT);
        String sd = sdf.format(new Date(timeDate));
        return sd;
    }

    /**
     * 自定义格式时间戳转换
     *
     * @param timeDate
     * @return
     */
    public static String timeStampToDate(long timeDate, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        String sd = sdf.format(new Date(timeDate));
        return sd;
    }

    public static boolean compare(String time1, String time2) throws ParseException {
        //如果想比较日期则写成"yyyy-MM-dd"就可以了
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm");
        //将字符串形式的时间转化为Date类型的时间
        Date a = sdf.parse(time1);
        Date b = sdf.parse(time2);
        //Date类的一个方法，如果a早于b返回true，否则返回false
        if (a.before(b)) {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 获取日期年份
     *
     * @param date 日期
     */

    public static int getYear(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_SHORT);
        return Integer.parseInt(sdf.format(date).substring(0, 4));
    }

    public static String getCurrentYear() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
        Date date = new Date();
        return sdf.format(date);
    }

    public static String getCurrentMonth() {
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        Date date = new Date();
        return sdf.format(date);
    }

    public static String getCurrentDay() {
        SimpleDateFormat sdf = new SimpleDateFormat("dd");
        Date date = new Date();
        return sdf.format(date);
    }


    /**
     * 功能描述：返回月
     *
     * @param date Date 日期
     * @return 返回月份
     */

    public static int getMonth(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        return calendar.get(Calendar.MONTH) + 1;
    }

    /**
     * 判断两个时间是否属于同一月
     */
    public static boolean isInSameMonth(long time1, long time2) {

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        Date date1 = new Date(time1);
        Date date2 = new Date(time2);

        calendar1.setTime(date1);
        calendar2.setTime(date2);

        return (calendar1.get(Calendar.MONTH) + 1) == (calendar2.get(Calendar.MONTH) + 1);
    }

    /**
     * 判断两个时间是否属于同一天
     */
    public static boolean isInSameDay(long time1, long time2) {

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();

        Date date1 = new Date(time1);
        Date date2 = new Date(time2);

        calendar1.setTime(date1);
        calendar2.setTime(date2);

        return  calendar1.get(Calendar.YEAR) == calendar2.get(Calendar.YEAR) &&
                calendar1.get(Calendar.DAY_OF_YEAR) == calendar2.get(Calendar.DAY_OF_YEAR);
    }

    /**
     * 判断该时间是否为当天
     *
     * @param time
     * @return
     */
    public static boolean isToday(long time) {
        Date date = new Date(time);
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
        if (fmt.format(date).toString().equals(fmt.format(new Date()).toString())) {//格式化为相同格式
            return true;
        } else {
            return false;
        }
    }

    /**
     * 功能描述：返回日
     *
     * @param date Date 日期
     * @return 返回日份
     */

    public static int getDay(Date date) {

        Calendar calendar = Calendar.getInstance();

        calendar.setTime(date);

        return calendar.get(Calendar.DAY_OF_MONTH);
    }

    //获得当前年月日时分秒星期
    public static String getTime(){
        final Calendar c = Calendar.getInstance();
        c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
        String mYear = String.valueOf(c.get(Calendar.YEAR)); // 获取当前年份
        String mMonth = String.valueOf(c.get(Calendar.MONTH) + 1);// 获取当前月份
        String mDay = String.valueOf(c.get(Calendar.DAY_OF_MONTH));// 获取当前月份的日期号码
        String mWay = String.valueOf(c.get(Calendar.DAY_OF_WEEK));
        String mHour = String.valueOf(c.get(Calendar.HOUR_OF_DAY));//时
        String mMinute = String.valueOf(c.get(Calendar.MINUTE));//分
        // String mSecond = String.valueOf(c.get(Calendar.SECOND));//秒

        if("1".equals(mWay)){
            mWay ="天";
        }else if("2".equals(mWay)){
            mWay ="一";
        }else if("3".equals(mWay)){
            mWay ="二";
        }else if("4".equals(mWay)){
            mWay ="三";
        }else if("5".equals(mWay)){
            mWay ="四";
        }else if("6".equals(mWay)){
            mWay ="五";
        }else if("7".equals(mWay)){
            mWay ="六";
        }
        //  return mYear + "年" + mMonth + "月" + mDay+"日"+"  "+"星期"+mWay+"  "+mHour+":"+mMinute+":"+mSecond;
        try {
            if(Integer.parseInt(mMonth)<10){
                mMonth="0"+mMonth;
            }
            if(Integer.parseInt(mDay)<10){
                mDay="0"+mDay;
            }
        }catch (Exception e){
            Log.e("Tag","mMonth："+mMonth+"+++++mDay："+mDay);
        }
        return mYear + "年" + mMonth + "月" + mDay+"日"+"  "+"星期"+mWay+"  "+mHour+":"+mMinute;
    }

    /**
     * 获取某年某月 天数
     *
     * @param year
     * @param month
     * @return
     */
    public static int getMonthDays(int year, int month) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        int n = c.getActualMaximum(Calendar.DAY_OF_MONTH);
        Log.d(Constants.APP_TAG, year + "年" + month + "月有" + n + "天");
        return n;
    }

    public static long getTodayTimeStamp() {
        Calendar c = Calendar.getInstance();
        c.setTime(new Date(System.currentTimeMillis()));
        c.set(Calendar.YEAR, c.get(Calendar.YEAR));
        c.set(Calendar.MONTH, c.get(Calendar.MONTH));
        c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH));
        //将小时至0
        c.set(Calendar.HOUR_OF_DAY, 0);
        //将分钟至0
        c.set(Calendar.MINUTE, 0);
        //将秒至0
        c.set(Calendar.SECOND, 0);
        //将毫秒至0
        c.set(Calendar.MILLISECOND, 0);
        return c.getTimeInMillis();
    }

    /**
     * 获取某一天是星期几
     *
     * @return 返回值为0的话为周日 返回值为6的话为周六
     */
    public static int getBeginDayOfMonthIndex(long time) {
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(new Date(time));
        Log.d(Constants.APP_TAG, "当前天是周" + (gc.get(Calendar.DAY_OF_WEEK) - 1));
        return gc.get(Calendar.DAY_OF_WEEK) - 1;
    }

    /**
     * 获取指定年月开始日期时间戳(比如6月1日)
     *
     * @return
     */
    public static Long getMonthBegin(int year, int month) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        //设置为1号,当前日期既为本月第一天
        c.set(Calendar.DAY_OF_MONTH, 1);
        //将小时至0
        c.set(Calendar.HOUR_OF_DAY, 0);
        //将分钟至0
        c.set(Calendar.MINUTE, 0);
        //将秒至0
        c.set(Calendar.SECOND, 0);
        //将毫秒至0
        c.set(Calendar.MILLISECOND, 0);
        // 获取本月第一天的时间戳
        return c.getTimeInMillis();
    }

    /**
     * 获取指定年月日时间戳
     *
     * @return
     */
    public static Long getTimeStamp(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        //设置为1号,当前日期既为本月第一天
        c.set(Calendar.DAY_OF_MONTH, day);
        //将小时至0
        c.set(Calendar.HOUR_OF_DAY, 0);
        //将分钟至0
        c.set(Calendar.MINUTE, 0);
        //将秒至0
        c.set(Calendar.SECOND, 0);
        //将毫秒至0
        c.set(Calendar.MILLISECOND, 0);
        // 获取本月第一天的时间戳
        return c.getTimeInMillis();
    }

    public static Long getTimeStamp2(int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month - 1);
        //设置为1号,当前日期既为本月第一天
        c.set(Calendar.DAY_OF_MONTH, day);
        //将小时至0
        c.set(Calendar.HOUR_OF_DAY, 0);
        //将分钟至0
        c.set(Calendar.MINUTE, 0);
        //将秒至0
        c.set(Calendar.SECOND, 0);
        //将毫秒至0
        c.set(Calendar.MILLISECOND, 0);
        // 获取本月第一天的时间戳
        return c.getTimeInMillis();
    }
}
