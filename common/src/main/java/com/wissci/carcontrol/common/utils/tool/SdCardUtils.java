package com.wissci.carcontrol.common.utils.tool;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static android.os.Environment.MEDIA_MOUNTED;

/**
 * @author zyp.
 * @data 2019-07-02.
 */
public class SdCardUtils {

    /**
     * bitmap文件保存到sd卡
     */
    public static File saveBmToSD(Bitmap mBitmap,String localPath) throws IOException {
        File file = new File(localPath);
        deleteFolder(file.getPath());
        file.createNewFile();
        FileOutputStream out = new FileOutputStream(file);
        if (mBitmap == null) {
            return null;
        }
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 50, out);
        out.flush();
        out.close();
        return file;
    }

    public static boolean deleteFolder(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            return false;
        } else {
            if (file.isFile()) {
                // 为文件时调用删除文件方法
                return deleteFile(filePath);
            } else {
                // 为目录时调用删除目录方法
                return deleteDirectory(filePath);
            }
        }
    }

    public static boolean deleteFile(String filePath) {
        File file = new File(filePath);
        if (file.isFile() && file.exists()) {
            return file.delete();
        }
        return false;
    }

    /**
     * 删除文件夹以及目录下的文件
     *
     * @param filePath 被删除目录的文件路径
     * @return 目录删除成功返回true，否则返回false
     */
    public static boolean deleteDirectory(String filePath) {
        boolean flag = false;
        //如果filePath不以文件分隔符结尾，自动添加文件分隔符
        if (!filePath.endsWith(File.separator)) {
            filePath = filePath + File.separator;
        }
        File dirFile = new File(filePath);
        if (!dirFile.exists() || !dirFile.isDirectory()) {
            return false;
        }
        flag = true;
        File[] files = dirFile.listFiles();
        //遍历删除文件夹下的所有文件(包括子目录)
        for (int i = 0; i < files.length; i++) {
            if (files[i].isFile()) {
                //删除子文件
                flag = deleteFile(files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            } else {
                //删除子目录
                flag = deleteDirectory(files[i].getAbsolutePath());
                if (!flag) {
                    break;
                }
            }
        }
        if (!flag) {
            return false;
        }
        //删除当前空目录
        return dirFile.delete();
    }

    /**
     * app对应文件存储在内部文件夹下,app卸载即删除
     *
     * @param context
     * @param dir     /tenantId/xdface/face 人脸绑定文件夹
     *                /tenantId/xdface/db   人脸数据库文件夹
     *                /tenantId/order       订单照片文件夹
     *                /tenantId/xdface/head 绑定人脸照相机返回照片文件夹
     * @return
     */
    public static String getFilePath(Context context, String dir) {
        String directoryPath;
        if (MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            directoryPath = context.getExternalFilesDir(dir).getAbsolutePath();
        } else {
            directoryPath = context.getFilesDir() + File.separator + dir;
        }
        File file = new File(directoryPath);
        if (!file.exists()) {
            file.mkdirs();
        }
        LogUtil.i("filePath====>" + directoryPath);
        return directoryPath;
    }
}
