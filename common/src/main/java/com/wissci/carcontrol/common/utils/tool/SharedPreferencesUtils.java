package com.wissci.carcontrol.common.utils.tool;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

/**
 * Created By: lsw
 * Author : lsw
 * Date :  2016/3/16
 * Email : lsw
 */
public class SharedPreferencesUtils {

	private SharedPreferences sharedPreferences;
	private static SharedPreferencesUtils appSharedPreferences;

	private SharedPreferencesUtils(Context context) {
		sharedPreferences = context.getSharedPreferences("xd_auto", Activity.MODE_PRIVATE);
	}

	public static SharedPreferencesUtils getInstance(Context context) {
		if (null == appSharedPreferences) {
			appSharedPreferences = new SharedPreferencesUtils(context);
		}
		return appSharedPreferences;
	}

	public String get(String key) {
		if (null == sharedPreferences) {
			return "";
		}
		String result = sharedPreferences.getString(key, "");
		if (!TextUtils.isEmpty(result) && result != null) {
			return result;
		}
		return "";
	}
	public String get(String key,String value) {
		if (null == sharedPreferences) {
			return "";
		}
		String result = sharedPreferences.getString(key, value);
		if (!TextUtils.isEmpty(result) && result != null) {
			return result;
		}
		return value;
	}
	public void set(String key, String value) {
		if (null != sharedPreferences) {
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putString(key, value);
			editor.commit();
		}
	}

	public Boolean getBoolean(String key) {
		if (null == sharedPreferences) {
			return false;
		}
		return sharedPreferences.getBoolean(key,false);
	}

	public Boolean getBoolean(String key,boolean defaultB ) {
		if (null == sharedPreferences) {
			return false;
		}
		return sharedPreferences.getBoolean(key,defaultB);
	}

	public Boolean getBooleanAlpha(String key) {
		if (null == sharedPreferences) {
			return false;
		}
		return sharedPreferences.getBoolean(key,true);
	}

	public void setBoolean(String key, Boolean value) {
		if (null != sharedPreferences) {
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putBoolean(key, value);
			editor.commit();
		}
	}

	public int getInt(String key) {
		if (null == sharedPreferences) {
			return 0;
		}
		return sharedPreferences.getInt(key,0);
	}
	public int getInt(String key ,int value) {
		if (null == sharedPreferences) {
			return 0;
		}
		return sharedPreferences.getInt(key,value);
	}

	public void setInt(String key, int value) {
		if (null != sharedPreferences) {
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putInt(key, value);
			editor.commit();
		}
	}



	public void remove(String... key) {
		for (String k : key) {
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.remove(k);
			editor.commit();
		}
	}

	public void setLong(String key, Long value) {
		if (null != sharedPreferences) {
			SharedPreferences.Editor editor = sharedPreferences.edit();
			editor.putLong(key, value);
			editor.commit();
		}
	}

    public long getLong(String key, long l) {
		if (null == sharedPreferences) {
			return 0L;
		}
		return sharedPreferences.getLong(key,l);
    }




}
