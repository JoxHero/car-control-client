package com.wissci.carcontrol.common.utils.tool;

import android.graphics.Bitmap;
import android.view.View;

/**
 * @author zyp.
 * @data 2020/3/16.
 */
public class ScreenShotUtils {

    public static Bitmap createBitmap(View view) {
        view.buildDrawingCache();
        Bitmap bitmap = view.getDrawingCache();
        return bitmap;
    }
}
