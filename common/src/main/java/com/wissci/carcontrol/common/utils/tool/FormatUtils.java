package com.wissci.carcontrol.common.utils.tool;

import android.annotation.SuppressLint;

import java.math.BigDecimal;
import java.text.DecimalFormat;


/**
 * @author zyp.
 * @data 2019/4/20.
 */
public class FormatUtils {

    @SuppressLint("NewApi")
    public static String formatInteger(double d){
        BigDecimal bigDecimal = new BigDecimal(d);
        bigDecimal.setScale(0,BigDecimal.ROUND_HALF_UP);
        return bigDecimal.toString();
    }

    @SuppressLint("NewApi")
    public static String formatHoldTwo(double d){
        DecimalFormat df1 = new DecimalFormat("0.00");
        String str = df1.format(d);
        return str;
    }

    public static String formatName(String name){
        if (name.length() > 1){
            char[] namechars = name.toCharArray();
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0;i<name.length();i++){
                if (i == 0){
                    stringBuilder.append(namechars[i]);
                    continue;
                }
                stringBuilder.append("*");
            }
            return stringBuilder.toString();
        }
        return name;
    }
}
