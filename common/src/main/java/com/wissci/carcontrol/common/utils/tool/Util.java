package com.wissci.carcontrol.common.utils.tool;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {
    private static final Pattern MOBILE_PATTERN = Pattern.compile("^(0|86|17951)?(13[0-9]|14[5-9]|15[0-3,5-9]|16[2,5,6,7]|17[0-8]|18[0-9]|19[1,3,5,8,9])\\d{8}$");
    private static final Pattern PASSWORD_PATTERN = Pattern.compile("^[a-zA-Z0-9]{6,20}$");
    private static final Pattern CHINESE_PATTERN = Pattern.compile("^[\\u4e00-\\u9fa5]+$");
    private static final Pattern PLATE_NUMBER_PATTERN = Pattern.compile("^([京津晋冀蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼渝川贵云藏陕甘青宁新][ABCDEFGHJKLMNPQRSTUVWXY][1-9DF][1-9ABCDEFGHJKLMNPQRSTUVWXYZ]\\d{3}[1-9DF]|[京津晋冀蒙辽吉黑沪苏浙皖闽赣鲁豫鄂湘粤桂琼渝川贵云藏陕甘青宁新][ABCDEFGHJKLMNPQRSTUVWXY][\\dABCDEFGHJKLNMxPQRSTUVWXYZ]{5})$");
    /**
     * 手机号校验（正则表达式）
     *
     * @param mobiles
     * @return
     */
    public static boolean ismobileNO(String mobiles) {
        mobiles = mobiles.replaceAll(" ", "");
        Matcher m = MOBILE_PATTERN.matcher(mobiles);
        return m.matches();
    }

    public static boolean isPlateNumber(String plateNumber){
        plateNumber = plateNumber.replaceAll(" ", "");
        Matcher m = PLATE_NUMBER_PATTERN.matcher(plateNumber);
        return m.matches();
    }

    public static String formatPhone(String pNumber) {
        if (!TextUtils.isEmpty(pNumber) && pNumber.length() > 6) {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pNumber.length(); i++) {
                char c = pNumber.charAt(i);
                if (i >= 3 && i <= 6) {
                    sb.append('*');
                } else {
                    sb.append(c);
                }
            }

            return sb.toString();
        }
        return "";
    }

    /**
     * 密码校验（正则表达式）
     *
     * @param password
     * @return
     */
    public static boolean isPassword(String password) {
        Matcher m = PASSWORD_PATTERN.matcher(password);
        return m.matches();
    }

    /**
     * 验证输入的名字是否为“中文”
     */
    public static boolean verifyName(String name) {
        Matcher m = CHINESE_PATTERN.matcher(name);
        return m.matches();
    }

    public static int getSDK_Version() {
        return Build.VERSION.SDK_INT;
    }

    public static boolean checkVersion(String serverVersionName, String versionNameSelf) {
        if (serverVersionName.contains("v") || serverVersionName.contains("V")) {
            serverVersionName = serverVersionName.substring(1);
        }

        //LogUtil.e("serverVersionName -- " + serverVersionName);

        String[] split = serverVersionName.split("\\.");

        String[] split1 = versionNameSelf.split("\\.");

        if (CheckString(split[0], split1[0])) {
            return true;
        } else {
            if (CheckString(split[1], split1[1])) {
                return true;
            } else {
                return CheckString(split[2], split1[2]);
            }
        }
    }

    private static boolean CheckString(String s, String s1) {
        return Integer.parseInt(s) > Integer.parseInt(s1);
    }

    public static boolean isUpgrade(String serverVersion, String localVersion) {
        int upgradeVersionIndex = compareVersion(serverVersion, localVersion);

        if (upgradeVersionIndex == 0){
            return false;
        }

        if (upgradeVersionIndex == 1){
            return true;
        }

        if (upgradeVersionIndex == -1){
            return false;
        }
        return false;
    }

    /**
     * 版本号比较
     *
     * @param serverVersion 服务器版本
     * @param localVersion  本地版本
     * @return 0代表相等，
     * 1代表version1大于version2，
     * -1代表version1小于version2
     */
    public static int compareVersion(String serverVersion, String localVersion) {
        if (serverVersion.equals(localVersion)) {
            return 0;
        }

        if (serverVersion.contains("v") || serverVersion.contains("V")) {
            serverVersion = serverVersion.substring(1);
        }

        String[] version1Array = serverVersion.split("\\.");
        String[] version2Array = localVersion.split("\\.");
        int index = 0;
        // 获取最小长度值
        int minLen = Math.min(version1Array.length, version2Array.length);
        int diff = 0;
        // 循环判断每位的大小
        while (index < minLen
                && (diff = Integer.parseInt(version1Array[index])
                - Integer.parseInt(version2Array[index])) == 0) {
            index++;
        }
        if (diff == 0) {
            // 如果位数不一致，比较多余位数
            for (int i = index; i < version1Array.length; i++) {
                if (Integer.parseInt(version1Array[i]) > 0) {
                    return 1;
                }
            }

            for (int i = index; i < version2Array.length; i++) {
                if (Integer.parseInt(version2Array[i]) > 0) {
                    return -1;
                }
            }
            return 0;
        } else {
            return diff > 0 ? 1 : -1;
        }
    }

    private static File mPhotoFile = null;

    public static void setPhotoFile(Context context, File photoFile, String fileName) {
        mPhotoFile = photoFile;
        // 其次把文件插入到系统图库
        try {
            MediaStore.Images.Media.insertImage(context.getContentResolver(),
                    mPhotoFile.getAbsolutePath(), fileName, null);
        } catch (FileNotFoundException e) {

            e.printStackTrace();
        }

        // 最后通知图库更新
        // context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));
        context.sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(photoFile.getPath()))));
    }

    public static File getPhotoFile() {

        return mPhotoFile;
    }


    /**
     * 保存图片到图库
     *
     * @param bmp
     */
    public static void saveImageToGallery(Context context, Bitmap bmp, String bitName) {
        // 首先保存图片
        File appDir = new File(Environment.getExternalStorageDirectory(),
                "yingtan");
        if (!appDir.exists()) {
            appDir.mkdir();
        }

        String fileName = bitName + ".jpg";
        File file = new File(appDir, fileName);

        try {
            FileOutputStream fos = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        setPhotoFile(context, file, bitName);
    }

    /**
     * @param bmp     获取的bitmap数据
     * @param picName 自定义的图片名
     */
    public static void saveBmp2Gallery(Context context, Bitmap bmp, String picName) {
        saveImageToGallery(context, bmp, picName);
        String fileName = null;
        //系统相册目录
        String galleryPath = Environment.getExternalStorageDirectory()
                + File.separator + Environment.DIRECTORY_DCIM
                + File.separator + "Camera" + File.separator;


        // 声明文件对象
        File file = null;
        // 声明输出流
        FileOutputStream outStream = null;
        try {
            // 如果有目标文件，直接获得文件对象，否则创建一个以filename为名称的文件
            file = new File(galleryPath, picName + ".jpg");
            // 获得文件相对路径
            fileName = file.toString();
            // 获得输出流，如果文件中有内容，追加内容
            outStream = new FileOutputStream(fileName);
            if (null != outStream) {
                bmp.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
            }
        } catch (Exception e) {
            e.getStackTrace();
        } finally {
            try {
                if (outStream != null) {
                    outStream.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        MediaStore.Images.Media.insertImage(context.getContentResolver(), bmp, fileName, null);
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        Uri uri = Uri.fromFile(file);
        intent.setData(uri);
        context.sendBroadcast(intent);

        ToastUtil.showToast(context, "图片保存成功");

    }

    /**
     * 弹起软键盘
     *
     * @param editText
     */
    public static void openKeyBoard(final Application context, final EditText editText) {
        editText.setFocusable(true);
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                InputMethodManager imm =
                        (InputMethodManager) context
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(editText, 0);
                editText.setSelection(editText.getText().length());
            }
        }, 200);

    }

    public static String sHA1(Context context){
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(
                    context.getPackageName(), PackageManager.GET_SIGNATURES);
            byte[] cert = info.signatures[0].toByteArray();
            MessageDigest md = MessageDigest.getInstance("SHA1");
            byte[] publicKey = md.digest(cert);
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < publicKey.length; i++) {
                String appendString = Integer.toHexString(0xFF & publicKey[i])
                        .toUpperCase(Locale.US);
                if (appendString.length() == 1)
                    hexString.append("0");
                hexString.append(appendString);
                hexString.append(":");
            }
            String result = hexString.toString();
            return result.substring(0, result.length()-1);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
