package com.wissci.carcontrol.common.utils.tool;
import android.util.Log;
import com.zlw.exchangeclient.common.BuildConfig;


/**
 * Created by Administrator on 2017/11/7.
 */

public class LogUtil {
    private static final String TAG = "HSDS";

    public static void e(String msg){
        if(BuildConfig.DEBUG){
            Log.e(TAG," --> "+ msg);
        }
    }
    public static void w(String msg){
        if(BuildConfig.DEBUG){
            Log.w(TAG,msg);
        }
    }

    public static void e(Exception e) {
        if(BuildConfig.DEBUG){
            Log.w(TAG, e.getMessage());
        }
    }

    public static void i(String msg) {
        if(BuildConfig.DEBUG){
            Log.i(TAG,msg);
        }
    }

    public static void d(String msg) {
        if(BuildConfig.DEBUG){
            Log.d(TAG,msg);
        }
    }

    public static void e(String tag,String msg){
        if(BuildConfig.DEBUG){
            Log.e(tag," --> "+ msg);
        }
    }
    public static void w(String tag,String msg){
        if(BuildConfig.DEBUG){
            Log.w(tag," --> "+ msg);
        }
    }

    public static void i(String tag,String msg){
        if(BuildConfig.DEBUG){
            Log.i(tag," --> "+ msg);
        }
    }

    public static void d(String tag,String msg){
        if(BuildConfig.DEBUG){
            Log.d(tag," --> "+ msg);
        }
    }
}
