package com.wissci.carcontrol.common.utils.tool;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;
import com.wissci.carcontrol.common.Constants;

/**
 * @Description: Created by yongpengzhang on 2021/8/1
 */
public class NetWorkUtils {
  public static void getNetWorkInfo(Context context) {
    if (!NetWorkUtils.isOnline(context)) {
      Constants.NetWorkState.NETWORK_TYPE = Constants.NetWorkState.NONE_TYPE;
      Constants.NetWorkState.isOnline = false;
      return;
    }
    Constants.NetWorkState.isOnline = true;
    if (getNetType(context).equals(Constants.NetWorkState.WIFI_NAME)) {
      WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
      final WifiInfo wifiInfo = wifiManager.getConnectionInfo();
      Constants.NetWorkState.WIFI_SSID = wifiInfo.getSSID();
      Constants.NetWorkState.NETWORK_TYPE = Constants.NetWorkState.WIFI_TYPE;
    } else if (getNetType(context).equals(Constants.NetWorkState.MOBILE_NAME)) {
      Constants.NetWorkState.NETWORK_TYPE = Constants.NetWorkState.MOBILE_TYPE;
    }
  }

  public static boolean isOnline(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetworkInfo = cm.getActiveNetworkInfo();
    return activeNetworkInfo != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected();
  }

  public static String getNetType(Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = cm.getActiveNetworkInfo();
    return networkInfo.getTypeName();
  }
}
