package com.wissci.carcontrol.common;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class Constants {
    public static final String APP_TAG = "hea";
    public static final String APP_DATA_TRANS = "app_data_trans";


    public static final String HOST = "172.123.90.1";
    public static final int PORT = 9555;
    public static final String WIFI_NAME = "test";

    public static final String APP_NAME = "AC_Pad";
    public static final String IS_FIRST = "is_first";
    //hand
    public static final byte[] HAND_HOT = {(byte) 0X7E, (byte) 0X55, (byte) 0XD8, (byte) 0XE7};
    //heat
    public static final byte[] ORDER_HEAT_DATA = {0X7E, (byte) 0XAA, 0X2B, (byte) 0XE7};
    //off
    public static final byte[] ORDER_MAIN_HOT_OFF = {0X7E, (byte) 0XEE, (byte) 0XF0, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_COOL_OFF = {0X7E, (byte) 0XDE, (byte) 0X60, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_MASSAGE_OFF = {0X7E, (byte) 0XCE, (byte) 0X10, (byte) 0XE7};

    public static final byte[] ORDER_SIDE_HOT_OFF = {0X7E, (byte) 0X05, (byte) 0X6F, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_COOL_OFF = {0X7E, (byte) 0X04, (byte) 0X68, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_MASSAGE_OFF = {0X7E, (byte) 0X03, (byte) 0X7D, (byte) 0XE7};
    //hot
    public static final byte[] ORDER_MAIN_HOT_LOW = {0X7E, 0X13, 0X0D, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_HOT_MID = {0X7E, 0X12, 0X0A, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_HOT_HIGH = {0X7E, 0X11, 0X03, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_HOT_LOW = {0X7E, 0X03, 0X7D, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_HOT_MID = {0X7E, 0X02, 0X7A, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_HOT_HIGH = {0X7E, 0X01, 0X73, (byte) 0XE7};
    //massage
    public static final byte[] ORDER_MAIN_MASSAGE_LOW = {0X7E, 0X1B, 0X35, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_MASSAGE_MID = {0X7E, 0X1A, 0X32, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_MASSAGE_HIGH = {0X7E, 0X19, 0X3B, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_MASSAGE_LOW = {0X7E, 0X0B, 0X45, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_MASSAGE_MID = {0X7E, 0X0A, 0X42, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_MASSAGE_HIGH = {0X7E, 0X09, 0X4B, (byte) 0XE7};
    //cool
    public static final byte[] ORDER_MAIN_COOL_LOW = {0X7E, 0X16, 0X16, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_COOL_MID = {0X7E, 0X17, 0X11, (byte) 0XE7};
    public static final byte[] ORDER_MAIN_COOL_HIGH = {0X7E, 0X18, 0X3C, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_COOL_LOW = {0X7E, 0X06, 0X66, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_COOL_MID = {0X7E, 0X07, 0X61, (byte) 0XE7};
    public static final byte[] ORDER_SIDE_COOL_HIGH = {0X7E, 0X08, 0X4C, (byte) 0XE7};

    public static List<Integer> MAIN_HOT = new ArrayList<>();
    public static List<Integer> MAIN_AIR = new ArrayList<>();
    public static List<Integer> MAIN_MASSAGE = new ArrayList<>();
    public static List<Integer> SIDE_HOT = new ArrayList<>();
    public static List<Integer> SIDE_AIR = new ArrayList<>();
    public static List<Integer> SIDE_MASSAGE = new ArrayList<>();

    public static volatile int MAIN_USING_FUNCTION = -1;
    public static volatile int SIDE_USING_FUNCTION = -1;

    public static boolean IS_CONNECTED = false;
    public static int LAST_TYPE = -2;

    public static class Status{
        /**
         * 有设备正在连接热点
         */
        public static final int DEVICE_CONNECTING = 1;
        /**
         * 有设备连上热点
         */
        public static final int DEVICE_CONNECTED = 2;
        /**
         * 发送消息成功
         */
        public static final int SEND_MSG_SUCCSEE = 3;
        /**
         * 发送消息失败
         */
        public static final int SEND_MSG_ERROR = 4;
        /**
         * 获取新消息
         */
        public static final int GET_MSG = 6;
        /**
         * 设备连接热点失败
         */
        public static final int DEVICE_CONNECT_FAIL = 7;
    }

    public static class NetWorkState {

        public static final String WIFI_NAME = "WIFI";
        public static final String MOBILE_NAME = "MOBILE";
        public static final String NONE = "none";

        public static final int WIFI_TYPE= 2;
        public static final int MOBILE_TYPE = 1;
        public static final int NONE_TYPE = 0;
        //连接wifi的名称
        public static String WIFI_SSID;
        //网络状态
        public static int NETWORK_TYPE; //0:NULL 1:MOBILE 2:WIFI
        public static boolean isOnline;
        public static String networkTypeName;
    }
}
