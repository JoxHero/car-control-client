package com.wissci.carcontrol.common.event;

/**
 * @Description: Created by yongpengzhang on 2021/8/1
 */
public class StatusEvent {
  private int state;
  private String message;

  public StatusEvent(int state, String message) {
    this.state = state;
    this.message = message;
  }

  public int getState() {
    return state;
  }

  public String getMessage() {
    return message;
  }
}
