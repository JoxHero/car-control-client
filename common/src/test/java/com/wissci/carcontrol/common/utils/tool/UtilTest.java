package com.wissci.carcontrol.common.utils.tool;


import org.junit.Assert;
import org.junit.Test;

/**
 * @author zyp.
 * @data 2019-11-14.
 */
public class UtilTest {

    public static boolean checkVersion(String serverVersionName, String versionNameSelf) {
        if (serverVersionName.contains("v") || serverVersionName.contains("V")) {
            serverVersionName = serverVersionName.substring(1);
        }

        //LogUtil.e("serverVersionName -- " + serverVersionName);

        String[] split = serverVersionName.split("\\.");

        String[] split1 = versionNameSelf.split("\\.");

        if (CheckString(split[0], split1[0])) {
            return true;
        } else {
            if (CheckString(split[1], split1[1])) {
                return true;
            } else {
                return CheckString(split[2], split1[2]);
            }
        }
    }

    private static boolean CheckString(String s, String s1) {
        return Integer.parseInt(s) > Integer.parseInt(s1);
    }

     @Test
     public void checkVersion() {
        boolean isCheck = checkVersion("V1.0.2","V1.1.0");
        Assert.assertTrue(isCheck);
    }
}