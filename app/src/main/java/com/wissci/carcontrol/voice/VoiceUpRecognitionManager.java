package com.wissci.carcontrol.voice;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.GrammarListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechEvent;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.VoiceWakeuper;
import com.iflytek.cloud.WakeuperListener;
import com.iflytek.cloud.WakeuperResult;
import com.iflytek.cloud.util.ResourceUtil;
import com.vise.baseble.ViseBle;
import com.vise.baseble.callback.IBleCallback;
import com.vise.baseble.callback.IConnectCallback;
import com.vise.baseble.common.PropertyType;
import com.vise.baseble.core.BluetoothGattChannel;
import com.vise.baseble.core.DeviceMirror;
import com.vise.baseble.core.DeviceMirrorPool;
import com.vise.baseble.exception.BleException;
import com.vise.baseble.model.BluetoothLeDevice;
import com.vise.baseble.utils.HexUtil;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.RxJava;
import com.wissci.carcontrol.event.VoiceChangeEvent;
import com.wissci.carcontrol.event.bluetooth.CallbackDataEvent;
import com.wissci.carcontrol.event.bluetooth.ConnectEvent;
import com.wissci.carcontrol.event.bluetooth.NotifyDataEvent;
import com.wissci.carcontrol.event.bluetooth.ScanEvent;
import com.wissci.carcontrol.output.Logger;
import com.wissci.carcontrol.utils.HandlerUtil;
import com.wissci.carcontrol.utils.JsonParser;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.LinkedList;
import java.util.Queue;
import java.util.UUID;

/**
 * 语音唤醒识别
 */
public class VoiceUpRecognitionManager {
    private static final String TAG = "Voice";
    public static final int OPEN_VENTILATION = 1;
    public static final int CLOSE_VENTILATION = 10;
    public static final int OPEN_HEAT = 2;
    public static final int CLOSE_HEAT = 20;
    public static final int OPEN_LIGHT = 3;
    public static final int CLOSE_LIGHT = 30;

    private static VoiceUpRecognitionManager instance;
    private static Context context;

    // 语音唤醒对象
    private VoiceWakeuper mIvw;
    // 语音识别对象
    private SpeechRecognizer mAsr;
    // 唤醒结果内容
    private String resultString;

    // 设置门限值 ： 门限值越低越容易被唤醒
    private final static int MAX = 3000;
    private final static int MIN = 0;
    private int curThresh = 1450;
    private String threshStr = "门限值：";
    // 云端语法文件
    private String mCloudGrammar = null;
    // 云端语法id
    private String mCloudGrammarID;
    // 本地语法id
    private String mLocalGrammarID;
    // 本地语法文件
    private String mLocalGrammar = null;
    // 本地语法构建路径
    private String grmPath;
    // 引擎类型
    private String mEngineType = SpeechConstant.TYPE_CLOUD;
    private Toast mToast;

    private VoiceUpRecognitionManager() {

    }

    public static VoiceUpRecognitionManager getInstance(Context context) {
        VoiceUpRecognitionManager.context = context;
        if (instance == null) {
            synchronized (VoiceUpRecognitionManager.class) {
                if (instance == null) {
                    instance = new VoiceUpRecognitionManager();
                }
            }
        }
        return instance;
    }

    public void init() {
        grmPath = context.getExternalFilesDir("msc").getAbsolutePath() + "/test";
        // 初始化唤醒对象
        mIvw = VoiceWakeuper.createWakeuper(context, null);
        // 初始化识别对象---唤醒+识别,用来构建语法
        mAsr = SpeechRecognizer.createRecognizer(context, null);
        // 初始化语法文件
        mCloudGrammar = readFile(context, "wake_grammar_sample.abnf", "utf-8");
        mLocalGrammar = readFile(context, "wake.bnf", "utf-8");

        setGrammar();
    }

    private void setParam(){
        // 非空判断，防止因空指针使程序崩溃
        mIvw = VoiceWakeuper.getWakeuper();
        if (mIvw != null) {
            resultString = "";
            final String resPath = ResourceUtil.generateResourcePath(context, ResourceUtil.RESOURCE_TYPE.assets, "ivw/" + context.getString(R.string.app_id) + ".jet");
            // 清空参数
            mIvw.setParameter(SpeechConstant.PARAMS, null);
            // 设置识别引擎
            mIvw.setParameter(SpeechConstant.ENGINE_TYPE, mEngineType);
            // 设置唤醒资源路径
            mIvw.setParameter(ResourceUtil.IVW_RES_PATH, resPath);
            /**
             * 唤醒门限值，根据资源携带的唤醒词个数按照“id:门限;id:门限”的格式传入
             * 示例demo默认设置第一个唤醒词，建议开发者根据定制资源中唤醒词个数进行设置
             */
            mIvw.setParameter(SpeechConstant.IVW_THRESHOLD, "0:"
                    + curThresh);
            // 设置唤醒+识别模式
            mIvw.setParameter(SpeechConstant.IVW_SST, "oneshot");
            // 设置返回结果格式
            mIvw.setParameter(SpeechConstant.RESULT_TYPE, "json");
//
//				mIvw.setParameter(SpeechConstant.IVW_SHOT_WORD, "0");
            mIvw.setParameter(SpeechConstant.KEEP_ALIVE,"1");
            // 设置唤醒录音保存路径，保存最近一分钟的音频
            mIvw.setParameter(SpeechConstant.IVW_AUDIO_PATH,
                    context.getExternalFilesDir("msc").getAbsolutePath() + "/ivw.wav");
            mIvw.setParameter(SpeechConstant.AUDIO_FORMAT, "wav");

            if (mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
                if (!TextUtils.isEmpty(mCloudGrammarID)) {
                    // 设置云端识别使用的语法id
                    mIvw.setParameter(SpeechConstant.CLOUD_GRAMMAR,
                            mCloudGrammarID);
                    mIvw.startListening(mWakeuperListener);
                } else {
                    showTip("请先构建语法");
                }
            } else {
                if (!TextUtils.isEmpty(mLocalGrammarID)) {
                    // 设置本地识别资源
                    mIvw.setParameter(ResourceUtil.ASR_RES_PATH,
                            getResourcePath());
                    // 设置语法构建路径
                    mIvw.setParameter(ResourceUtil.GRM_BUILD_PATH, grmPath);
                    // 设置本地识别使用语法id
                    mIvw.setParameter(SpeechConstant.LOCAL_GRAMMAR,
                            mLocalGrammarID);
                    mIvw.startListening(mWakeuperListener);
                } else {
                    showTip("请先构建语法");
                }
            }

        } else {
            showTip("唤醒未初始化");
        }
    }

    private void setGrammar(){
        int ret = 0;
        if (mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
            // 设置参数
            mAsr.setParameter(SpeechConstant.ENGINE_TYPE, mEngineType);
            mAsr.setParameter(SpeechConstant.TEXT_ENCODING, "utf-8");
            // 开始构建语法
            ret = mAsr.buildGrammar("abnf", mCloudGrammar, grammarListener);
            if (ret != ErrorCode.SUCCESS) {
                Log.d(TAG,"语法构建失败,错误码：" + ret + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
                //showTip("语法构建失败,错误码：" + ret + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
            }
        } else {
            mAsr.setParameter(SpeechConstant.PARAMS, null);
            mAsr.setParameter(SpeechConstant.TEXT_ENCODING, "utf-8");
            // 设置引擎类型
            mAsr.setParameter(SpeechConstant.ENGINE_TYPE, mEngineType);
            // 设置语法构建路径
            mAsr.setParameter(ResourceUtil.GRM_BUILD_PATH, grmPath);
            // 设置资源路径
            mAsr.setParameter(ResourceUtil.ASR_RES_PATH, getResourcePath());
            ret = mAsr.buildGrammar("bnf", mLocalGrammar, grammarListener);
            if (ret != ErrorCode.SUCCESS) {
                //showTip("语法构建失败,错误码：" + ret + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
                Log.d(TAG,"语法构建失败,错误码：" + ret + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
            }
        }

        RxJava.timer(2, () -> setParam());

    }

    GrammarListener grammarListener = new GrammarListener() {
        @Override
        public void onBuildFinish(String grammarId, SpeechError error) {
            if (error == null) {
                if (mEngineType.equals(SpeechConstant.TYPE_CLOUD)) {
                    mCloudGrammarID = grammarId;
                } else {
                    mLocalGrammarID = grammarId;
                }
                Log.d(TAG, "语法构建成功：" + grammarId);
            } else {
                Log.d(TAG, "语法构建失败,错误码：" + error.getErrorCode() + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
                //showTip("语法构建失败,错误码：" + error.getErrorCode() + ",请点击网址https://www.xfyun.cn/document/error-code查询解决方案");
            }
        }
    };

    private WakeuperListener mWakeuperListener = new WakeuperListener() {

        @Override
        public void onResult(WakeuperResult result) {
            try {
                String text = result.getResultString();
                JSONObject object;
                object = new JSONObject(text);
                StringBuffer buffer = new StringBuffer();
                buffer.append("【RAW】 " + text);
                buffer.append("\n");
                buffer.append("【操作类型】" + object.optString("sst"));
                buffer.append("\n");
                buffer.append("【唤醒词id】" + object.optString("id"));
                buffer.append("\n");
                buffer.append("【得分】" + object.optString("score"));
                buffer.append("\n");
                buffer.append("【前端点】" + object.optString("bos"));
                buffer.append("\n");
                buffer.append("【尾端点】" + object.optString("eos"));
                resultString = buffer.toString();
            } catch (JSONException e) {
                resultString = "结果解析出错";
                e.printStackTrace();
            }
            Log.d(TAG, "WakeuperResult onResult: " + resultString);
        }

        @Override
        public void onError(SpeechError error) {
            showTip(error.getPlainDescription(true));
        }

        @Override
        public void onBeginOfSpeech() {
            Log.d(TAG, "开始说话");
            //showTip("开始说话");
        }

        @Override
        public void onEvent(int eventType, int isLast, int arg2, Bundle obj) {
            Log.d(TAG, "eventType:" + eventType + "arg1:" + isLast + "arg2:" + arg2);
            // 识别结果
            if (SpeechEvent.EVENT_IVW_RESULT == eventType) {
                RecognizerResult reslut = ((RecognizerResult) obj.get(SpeechEvent.KEY_EVENT_IVW_RESULT));
                String recoString = JsonParser.parseGrammarResult(reslut.getResultString());

                Log.d(TAG, "WakeuperResult onEvent : " + recoString);
                int type = 0;
                if (recoString.contains("打开通风")){
                    type = OPEN_VENTILATION;
                }else if(recoString.contains("打开加热")){
                    type = OPEN_HEAT;
                }else if(recoString.contains("打开灯光")){
                    type = OPEN_LIGHT;
                }else if (recoString.contains("关闭通风")){
                    type = CLOSE_VENTILATION;
                }else if(recoString.contains("关闭加热")){
                    type = CLOSE_HEAT;
                }else if(recoString.contains("关闭灯光")){
                    type = CLOSE_LIGHT;
                }

                EventBus.getDefault().post(new VoiceChangeEvent(type));
            }
            mIvw.startListening(mWakeuperListener);
        }

        @Override
        public void onVolumeChanged(int volume) {
            // TODO Auto-generated method stub
        }
    };

    /**
     * 读取asset目录下文件。
     *
     * @return content
     */
    public static String readFile(Context mContext, String file, String code) {
        int len = 0;
        byte[] buf = null;
        String result = "";
        try {
            InputStream in = mContext.getAssets().open(file);
            len = in.available();
            buf = new byte[len];
            in.read(buf, 0, len);

            result = new String(buf, code);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    // 获取识别资源路径
    private String getResourcePath() {
        StringBuffer tempBuffer = new StringBuffer();
        // 识别通用资源
        tempBuffer.append(ResourceUtil.generateResourcePath(context,
                ResourceUtil.RESOURCE_TYPE.assets, "asr/common.jet"));
        return tempBuffer.toString();
    }

    private void showTip(final String str) {
        HandlerUtil.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mToast != null) {
                    mToast.cancel();
                }
                mToast = Toast.makeText(context, str, Toast.LENGTH_SHORT);
                mToast.show();
            }
        });
    }
}
