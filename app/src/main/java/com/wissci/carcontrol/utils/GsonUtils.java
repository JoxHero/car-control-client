package com.wissci.carcontrol.utils;

import com.google.gson.Gson;

import java.lang.reflect.Type;

public class GsonUtils {

    private GsonUtils() {
    }

    private static Gson mGson;

    static {
        mGson = new Gson();
    }

    public static String toJson(Object object) {
        return mGson.toJson(object);
    }

    public static String toJson(Object src, Type typeOfSrc) {
        return mGson.toJson(src, typeOfSrc);
    }

    public static <T> T fromJson(String json, Class<T> classOfT) {
        return mGson.fromJson(json, classOfT);
    }

    public static <T> T fromJson(String json, Type typeOfT) {
        return mGson.fromJson(json, typeOfT);
    }
}
