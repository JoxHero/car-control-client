package com.wissci.carcontrol.utils;

/**
 * @Description: Created by yongpengzhang on 2021/9/2
 */
public class HexUtils {
  /**
   * 16进制转2进制
   * @param data
   * @return
   */
  public static String hexadecimalToBinary(byte data){
    //int ten = Integer.parseInt(String.valueOf(data), 16);
    String two = Integer.toBinaryString(Math.abs(data));
    int len = 8 - two.length();
    //不够8位前面补0
    if (len > 0){
      StringBuilder stringBuilder = new StringBuilder();
      for (int i = 0 ;i<len;i++){
        stringBuilder.append("0");
      }
      stringBuilder.append(two);
      two = stringBuilder.toString();
    }
    return two;
  }

  public static String hexString(byte b) {
    String s = Integer.toHexString(b);
    if(s.length() < 2) {
      s = "0" + s;
    } else if (s.length() > 2) {
      s = s.substring(s.length() - 2, s.length());
    }
    return s.toUpperCase();
  }

  /**
   * 10进制转16进制
   */
  public static String decToHex(int n){
    String r="";//空字符串
    while (n>16){
      int yushu=n%16;
      int shang=n/16;
      if(yushu>9){//特殊处理
        char c=(char) ((yushu-10)+'A');
        r+=c;//连接字符c
      }else {
        r+=yushu;
      }
      n=shang;
    }
    if(n>9){
      char c=(char)((n-10)+'A');
      r+=c;
    }else {
      r+=n;
    }
    return reverse(r);
  }
  //反转字符串（反转后就是正确顺序的十六进制数：从下到上的顺序）
  private static String reverse(String r) {
    String s="";
    for(int i=r.length()-1;i>=0;i--){
      s+=r.charAt(i);
    }
    return s;
  }
}
