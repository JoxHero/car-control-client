package com.wissci.carcontrol.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.wissci.carcontrol.AppApplication;
import com.wissci.carcontrol.R;

public class SPUtils {
    private static final SharedPreferences mPreferences;
    private static final SharedPreferences.Editor mEditor;

    static {
        mPreferences = AppApplication.getContext()
                .getSharedPreferences(AppApplication.getContext().getString(R.string.application_name), Context.MODE_PRIVATE);
        mEditor = mPreferences.edit();
    }

    private SPUtils() {
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return mPreferences.getBoolean(key, defValue);
    }

    public static float getFloat(String key, float defValue) {
        return mPreferences.getFloat(key, defValue);
    }

    public static int getInt(String key, int defValue) {
        return mPreferences.getInt(key, defValue);
    }

    public static long getLong(String key, long defValue) {
        return mPreferences.getLong(key, defValue);
    }

    public static String getString(String key, String defValue) {
        return mPreferences.getString(key, defValue);
    }

    public static void commitBoolean(String key, boolean value) {
        mEditor.putBoolean(key, value).commit();
    }

    public static void commitInt(String key, int value) {
        mEditor.putInt(key, value).commit();
    }

    public static void commitFloat(String key, float value) {
        mEditor.putFloat(key, value).commit();
    }

    public static void commitLong(String key, long value) {
        mEditor.putLong(key, value).commit();
    }

    public static void commitString(String key, String value) {
        mEditor.putString(key, value).commit();
    }

    public static boolean contains(String key) {
        return mPreferences.contains(key);
    }

    public static void remove(String key) {
        mEditor.remove(key).commit();
    }
}
