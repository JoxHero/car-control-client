package com.wissci.carcontrol.utils;

/**
 * @Description: Created by yongpengzhang on 2021/8/13
 */
public class ByteConstants {
  //设备
  /**
   * 平板
   */
  public static final byte DEVICE_PAD = 0x01;
  /**
   * 气动板
   */
  public static final byte DEVICE_PNEUMATIC = 0x02;
  /**
   * 主控板
   */
  public static final byte DEVICE_MAIN_CONTROL = 0x03;


  /**
   * 帧头
   */
  public static final byte FRAME_HEADER = (byte)0xEB;
  public static final byte FRAME_HEADER2 = (byte)0x90;
  /**
   * 帧尾
   */
  public static final byte FRAME_END = (byte)0x90;
  public static final byte FRAME_END2 = (byte)0xEB;

  //功能号

  /**
   * 座椅人员信息
   */
  public static final byte FUNCTION_SEAT_PERSONNEL = 0x01;
  /**
   * 按摩
   */
  public static final byte FUNCTION_MASSAGE = 0x02;
  /**
   * 向电机调节
   */
  public static final byte FUNCTION_DIRECTION_MOTOR = 0x03;
  /**
   * 通风加热
   */
  public static final byte FUNCTION_VENTILATION_HEAT = 0x04;
  /**
   * RGB灯带
   */
  public static final byte FUNCTION_RGB_LIGHT_STRIP = 0x05;
  /**
   * 获取车速
   */
  public static final byte FUNCTION_GET_SPEED = 0x06;
  /**
   * 发送车速
   */
  public static final byte FUNCTION_SEND_SPEED = 0x07;


  public static class Seat{
    /**
     * 主驾 1号座椅
     */
    public static final byte SEAT_MAIN = 0x01;
    /**
     * 副驾 2号座椅
     */
    public static final byte SEAT_VICE = 0x02;
  }

  /**
   * 气囊
   */
  public static class Airbag{
    //气囊类型
    /**
     * 颈部气囊
     */
    public static final byte NECK_AIRBAG = 0x01;
    /**
     * 腰部气囊
     */
    public static final byte WAIST_AIRBAG = 0x02;
    /**
     * 腰部点气囊
     */
    public static final byte WAIST_POINT_AIRBAG = 0x03;
    /**
     * 颈部停止充气放气
     */
    public static final byte NECK_AIRBAG_GEAR_CLOSE = 0x00;
    /**
     * 颈部气囊充气
     */
    public static final byte NECK_AIRBAG_GEAR_INFLATE = 0x01;

    /**
     * 颈部气囊放气
     */
    public static final byte NECK_AIRBAG_GEAR_DEFLATE = 0x02;

    /**
     * 腰部停止充气放气
     */
    public static final byte WAIST_AIRBAG_0 = 0x00;
    /**
     * （上）3号充气
     */
    public static final byte WAIST_AIRBAG_1 = 0x01;
    /**
     * （下）1号充气
     */
    public static final byte WAIST_AIRBAG_2 = 0x02;
    /**
     * （右）1、2、3号充气
     */
    public static final byte WAIST_AIRBAG_3 = 0x03;
    /**
     * （左）1、2、3号放气
     */
    public static final byte WAIST_AIRBAG_4 = 0x04;

    /**
     * 腰部点气囊关闭
     */
    public static final byte WAIST_POINT_AIRBAG_CLOSE = 0x00;
    /**
     * 腰部点气囊-按摩模式1
     */
    public static final byte WAIST_POINT_AIRBAG_1 = 0x01;
    /**
     * 腰部点气囊-按摩模式2
     */
    public static final byte WAIST_POINT_AIRBAG_2 = 0x02;
    /**
     * 腰部点气囊-按摩模式3
     */
    public static final byte WAIST_POINT_AIRBAG_3 = 0x03;
    /**
     * 腰部点气囊-按摩模式4
     */
    public static final byte WAIST_POINT_AIRBAG_4 = 0x04;
    /**
     * 腰部点气囊-按摩模式5
     */
    public static final byte WAIST_POINT_AIRBAG_5 = 0x05;
  }

  /**
   * 电机
   */
  public static class Motor{
    /**
     * 电机类型-座椅前后
     */
    public static final byte MOTOR_TYPE_1 = 0x01;
    /**
     * 电机类型-座椅上下
     */
    public static final byte MOTOR_TYPE_2 = 0x02;
    /**
     * 电机类型-座椅倾斜
     */
    public static final byte MOTOR_TYPE_3 = 0x03;
    /**
     * 电机类型-脚托上下
     */
    public static final byte MOTOR_TYPE_4 = 0x04;
  }

  /**
   * 加热通风
   */
  public static class VentilationHeat{
    /**
     * 加热
     */
    public static final byte TYPE_HEAT = 0x01;
    /**
     * 通风
     */
    public static final byte TYPE_VENTILATION = 0x02;
    /**
     * Auto
     */
    public static final byte TYPE_AUTO = 0x03;

    /**
     * 加热、通风关闭
     */
    public static final byte VENTILATION_HEAT_CLOSE = 0x00;
    /**
     * 加热、通风 档位1
     */
    public static final byte VENTILATION_HEAT_1 = 0x01;
    /**
     * 加热、通风 档位2
     */
    public static final byte VENTILATION_HEAT_2 = 0x02;
    /**
     * 加热、通风 档位3
     */
    public static final byte VENTILATION_HEAT_3 = 0x03;
  }

  /**
   * RGB 灯带
   */
  public static class RgbLightStrip{
    /**
     * 关闭灯光
     */
    public static final byte RGB_LIGHT_STRIP_CLOSE = 0x00;
    /**
     * 白光
     */
    public static final byte RGB_LIGHT_STRIP_WHITE = 0x01;
    /**
     * 红光
     */
    public static final byte RGB_LIGHT_STRIP_RED = 0x02;
    /**
     * 绿光
     */
    public static final byte RGB_LIGHT_STRIP_GREEN = 0x03;
    /**
     * 蓝光
     */
    public static final byte RGB_LIGHT_STRIP_BLUE = 0x04;
  }

  public static byte TEMP_DATA = (byte) 0x20;

}
