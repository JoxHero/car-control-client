package com.wissci.carcontrol;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.vise.baseble.model.BluetoothLeDevice;
import com.vise.baseble.utils.HexUtil;
import com.wissci.carcontrol.bluetooth.BluetoothDeviceManager;
import com.wissci.carcontrol.common.event.StatusEvent;
import com.wissci.carcontrol.service.JobService;
import lombok.Getter;
import lombok.Setter;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 * @Description: Created by yongpengzhang on 2021/8/1
 */
public class MainViewModel extends ViewModel {
  private JobService jobService;
  @Getter @Setter private MutableLiveData<byte[]> sendMsg = new MutableLiveData<>();

  public MainViewModel() {
    EventBus.getDefault().register(this);
  }

  public void setJobService(JobService jobService) {
    this.jobService = jobService;
  }

  public void sendMsg(byte[] data) {
    sendMsg.setValue(data);

  }

  @Subscribe public void handleReceiverMsg(StatusEvent statusEvent) {

  }
}
