package com.wissci.carcontrol;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.jakewharton.processphoenix.ProcessPhoenix;
import com.squareup.picasso.Picasso;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;
import com.wissci.carcontrol.bluetooth.BluetoothDeviceManager;
import com.wissci.carcontrol.broadcastreceiver.NetChangeReceiver;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.utils.tool.NetWorkUtils;
import com.wissci.carcontrol.crash.CrashCatcher;
import com.wissci.carcontrol.di.AppComponent;
import com.wissci.carcontrol.di.DaggerAppComponent;
import com.wissci.carcontrol.event.NetWorkChangeEvent;
import com.wissci.carcontrol.event.NetWorkEvent;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import dagger.android.DispatchingAndroidInjector;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class AppApplication extends DaggerApplication{
    public static final String TAG = Constants.APP_TAG;
    private static Context mContext;
    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    private boolean inBackground = false;
    private Picasso picasso;
    private EventBus rxBus;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        if (ProcessPhoenix.isPhoenixProcess(getApplicationContext())){
            return;
        }

        if (!BuildConfig.DEBUG) {
            CrashCatcher crashCatcher = new CrashCatcher(this);
            Thread.setDefaultUncaughtExceptionHandler(crashCatcher);
        }
        NetWorkUtils.getNetWorkInfo(this);

        initUtility();
        initBluetooth();
        registerCallbacks();
        initializeUM();
        registerNetChangeReceiver();
    }

    public void initUtility(){
        StringBuffer param = new StringBuffer();
        param.append("appid=" + getString(R.string.app_id));
        param.append(",");
        // 设置使用v5+
        param.append(SpeechConstant.ENGINE_MODE + "=" + SpeechConstant.MODE_MSC);
        SpeechUtility.createUtility(getApplicationContext(), param.toString());
    }

    /**
     * 初始化蓝牙
     */
    public void initBluetooth(){
        BluetoothDeviceManager.getInstance().init(this);
    }

    public void initializeUM(){
        UMConfigure.init(this,"5e67316f570df318a1000291","UMENG_CHANNEL",UMConfigure.DEVICE_TYPE_PHONE,"");
        if (BuildConfig.DEBUG){
            UMConfigure.setLogEnabled(true);
        }else{
            UMConfigure.setLogEnabled(false);
        }
        MobclickAgent.setPageCollectionMode(MobclickAgent.PageMode.AUTO);
    }


    private NetChangeReceiver receiver;
    private NetChangeReceiver.NetWorkChange netWorkChange;
    private void registerNetChangeReceiver(){
        rxBus = EventBus.getDefault();
        netWorkChange = new NetChangeReceiver.NetWorkChange() {
            @Override
            public void netWorkConnected() {
                Log.d(TAG, "netWorkConnected: network is connect!");
                rxBus.post(new NetWorkEvent(true, Constants.NetWorkState.NETWORK_TYPE));
            }

            @Override
            public void netWorkDisconnected() {
                Log.d(TAG, "netWorkConnected: network is disconnect!!!");
                rxBus.post(new NetWorkEvent(false, Constants.NetWorkState.NONE_TYPE));
            }

            @Override public void netWorkChange() {
                rxBus.post(new NetWorkChangeEvent(true, Constants.NetWorkState.NETWORK_TYPE));
            }
        };
        receiver = new NetChangeReceiver(netWorkChange);
        this.registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    private void registerCallbacks() {
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            int currentlyStartedActivities = 0;

            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

            }

            @Override
            public void onActivityStarted(Activity activity) {
                currentlyStartedActivities++;
                Log.d(TAG, "onActivityStarted: currentlyStartedActivities = " + currentlyStartedActivities);
                inBackground = (currentlyStartedActivities == 0);
                if (currentlyStartedActivities == 1) {
                    //应用从后台到前台需要记录
                }
            }

            @Override
            public void onActivityResumed(Activity activity) {
                Log.d(TAG, "onActivityResumed: currentlyStartedActivities = " + currentlyStartedActivities);
            }

            @Override
            public void onActivityPaused(Activity activity) {
            }

            @Override
            public void onActivityStopped(Activity activity) {
                currentlyStartedActivities--;
                Log.d(TAG, "onActivityStopped: currentlyStartedActivities = " + currentlyStartedActivities);
                inBackground = (currentlyStartedActivities == 0);

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if (inBackground && activity instanceof MainActivity){
                    if (receiver != null){
                        unregisterReceiver(receiver);
                    }
                }
            }
        });
    }

    public boolean isInBackground() {
        return inBackground;
    }

    /**
     * method that kills the current process.
     * It is used after restarting or killing the app.
     */
    public static void killCurrentProcess() {
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(10);
    }

    public static Context getContext(){
        return mContext;
    }


}
