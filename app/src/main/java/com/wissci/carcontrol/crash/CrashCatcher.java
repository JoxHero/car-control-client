package com.wissci.carcontrol.crash;

import com.jakewharton.processphoenix.ProcessPhoenix;
import com.wissci.carcontrol.AppApplication;
import com.wissci.carcontrol.BuildConfig;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class CrashCatcher implements Thread.UncaughtExceptionHandler{

    private AppApplication application;
    private Thread.UncaughtExceptionHandler defaultHandler;

    public CrashCatcher(AppApplication application) {
        this.application = application;
        defaultHandler = Thread.getDefaultUncaughtExceptionHandler();
    }
    @Override
    public void uncaughtException(Thread thread, Throwable throwable) {
        if (BuildConfig.DEBUG && defaultHandler != null){
            defaultHandler.uncaughtException(thread, throwable);
            return;
        }

        throwable.printStackTrace();
        handleException(throwable);
    }

    private void handleException(Throwable e) {
        restart();
    }

    private void restart() {
        ProcessPhoenix.triggerRebirth(application.getApplicationContext());
    }

}
