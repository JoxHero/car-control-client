package com.wissci.carcontrol.view.ui.home;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.utils.VibrateHelp;

/**
 * @Description: 市价、限价、计划 二次下单确认
 * Created by yongpengzhang on 2021/7/31
 */
public class ScendLevelDialog extends DialogFragment {

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    View view = inflater.inflate(R.layout.view_second_level, container, false);
    startAnimation(view);
    long[] patter = {1000, 2000, 1000, 50};
    VibrateHelp.vComplicated(getActivity(),patter,0);
    return view;
  }

  private void startAnimation(View view){
    Animation translateAnimation = new TranslateAnimation(-20, 20, 0, 0);
    translateAnimation.setDuration(100);//每次时间
    translateAnimation.setRepeatCount(1000);//重复次数
    /**倒序重复REVERSE  正序重复RESTART**/
    translateAnimation.setRepeatMode(Animation.REVERSE);
    view.startAnimation(translateAnimation);
  }

  @Override public void dismiss() {
    super.dismiss();
    VibrateHelp.stop();
  }
}
