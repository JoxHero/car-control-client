package com.wissci.carcontrol.view.widget.tablayout.listener;

public interface OnTabSelectListener {
    void onTabSelect(int position);
    void onTabReselect(int position);
}