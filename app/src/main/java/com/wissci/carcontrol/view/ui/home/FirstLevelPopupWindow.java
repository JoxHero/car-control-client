package com.wissci.carcontrol.view.ui.home;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.R;
import java.util.List;
import org.jetbrains.annotations.NotNull;

/**
 * 选择弹窗
 */
public class FirstLevelPopupWindow extends PopupWindow {
    private final PadManager padManager;
    private Context context;
    private View popuView;
    private ImageView ivMain;
    private ImageView ivVice;

    public FirstLevelPopupWindow(Context context, PadManager padManager) {
        super(context);
        this.padManager = padManager;
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        popuView = LayoutInflater.from(context).inflate(R.layout.view_firist_level, null);
        ivMain = popuView.findViewById(R.id.iv_main);
        ivVice = popuView.findViewById(R.id.iv_vice);

        setContentView(popuView);

        //设置背景只有设置了这个才可以点击外边和BACK消失
        setBackgroundDrawable(new ColorDrawable());
        //设置点击外边可以消失
        setOutsideTouchable(true);
    }

    @Override public void showAsDropDown(View anchor, int xoff, int yoff) {
        if (ivMain == null || ivVice == null){
            return;
        }
        if (padManager.isMainPadHavePeople && !padManager.isMainSeatBelt){
            ivMain.setSelected(false);
        }else{
            ivMain.setSelected(true);
        }

        if (padManager.isVicePadHavePeople && !padManager.isViceSeatBelt){
            ivVice.setSelected(false);
        }else{
            ivVice.setSelected(true);
        }
        super.showAsDropDown(anchor, xoff, yoff);
    }
}
