package com.wissci.carcontrol.view.ui.home;


import com.wissci.carcontrol.base.presenter.LoadDataView;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public interface HomeView extends LoadDataView {

    void seatSetChange();

    void setVoiceChangeView(int type);
}
