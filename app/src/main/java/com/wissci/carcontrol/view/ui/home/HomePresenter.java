package com.wissci.carcontrol.view.ui.home;

import com.wissci.carcontrol.MainViewModel;
import com.wissci.carcontrol.base.presenter.BasePresenter;

import com.wissci.carcontrol.common.event.SettingSeatInfoChange;
import com.wissci.carcontrol.event.VoiceChangeEvent;
import com.wissci.carcontrol.utils.ByteConstants;
import com.wissci.carcontrol.voice.VoiceUpRecognitionManager;
import javax.inject.Inject;
import lombok.Setter;

import static com.wissci.carcontrol.view.ui.home.HomeFragment.DriverPostionHolder.POSTION_MAIN_DRIVER;

import android.util.Log;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class HomePresenter extends BasePresenter<HomeView> {
    @Setter
    private MainViewModel mainViewModel;

    @Inject
    public HomePresenter() {
        EventBus.getDefault().register(this);
    }

    /**
     * 加热通风调节
     * @param postion 位置
     * @param progress 档位
     */
    public void adjustVentilationHeat(byte type,int postion,int progress){
        byte[] sendMainControlBytes = new byte[] {
            ByteConstants.FRAME_HEADER,
            ByteConstants.FRAME_HEADER2,
            ByteConstants.DEVICE_MAIN_CONTROL,
            0,
            0,
            0,
            0,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.FRAME_END,
            ByteConstants.FRAME_END2};

        byte function = ByteConstants.FUNCTION_VENTILATION_HEAT;
        byte gear;
        byte postionNum;
        if (postion == POSTION_MAIN_DRIVER){
            postionNum = ByteConstants.Seat.SEAT_MAIN;
        }else{
            postionNum = ByteConstants.Seat.SEAT_VICE;
        }
        //设置档位
        if (progress == 0){
            gear = ByteConstants.VentilationHeat.VENTILATION_HEAT_CLOSE;
        }else if(progress == 1){
            gear = ByteConstants.VentilationHeat.VENTILATION_HEAT_1;
        }else if(progress == 2){
            gear = ByteConstants.VentilationHeat.VENTILATION_HEAT_2;
        }else if (progress == 3){
            gear = ByteConstants.VentilationHeat.VENTILATION_HEAT_3;
        }else{
            gear = ByteConstants.VentilationHeat.VENTILATION_HEAT_CLOSE;
        }
        sendMainControlBytes[3] = function;
        sendMainControlBytes[4] = type;
        sendMainControlBytes[5] = gear;
        sendMainControlBytes[6] = postionNum;
        mainViewModel.sendMsg(sendMainControlBytes);
    }

    /**
     * 灯带调节
     * @param postion 位置
     * @param progress 档位
     */
    public void adjustLight(int postion ,int progress){
        byte [] sendMainControlBytes = new byte[] {
            ByteConstants.FRAME_HEADER,
            ByteConstants.FRAME_HEADER2,
            ByteConstants.DEVICE_MAIN_CONTROL,
            0,
            0,
            0,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.FRAME_END,
            ByteConstants.FRAME_END2};

        byte function = ByteConstants.FUNCTION_RGB_LIGHT_STRIP;
        byte gear;
        byte postionNum;
        if (postion == POSTION_MAIN_DRIVER){
            postionNum = ByteConstants.Seat.SEAT_MAIN;
        }else{
            postionNum = ByteConstants.Seat.SEAT_VICE;
        }

        //设置档位
        if (progress == 0){
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_CLOSE;
        }else if(progress == 1){
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_WHITE;
        }else if(progress == 2){
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_RED;
        }else if (progress == 3){
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_GREEN;
        }else if (progress ==4 ){
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_BLUE;
        }else{
            gear = ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_CLOSE;
        }
        sendMainControlBytes[3] = function;
        sendMainControlBytes[4] = gear;
        sendMainControlBytes[5] = postionNum;

        mainViewModel.sendMsg(sendMainControlBytes);
    }

    /**
     * 向电机调节
     * @param motorType 电机类型
     * @param postion 位置
     * @param progress 档位 //电机位置
     */
    public synchronized void sendDirectionMotor(byte motorType,int postion,int progress){
        byte [] sendMainControlBytes = new byte[] {
            ByteConstants.FRAME_HEADER,
            ByteConstants.FRAME_HEADER2,
            ByteConstants.DEVICE_MAIN_CONTROL,
            0,
            0,
            0,
            0,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.FRAME_END,
            ByteConstants.FRAME_END2};

        byte function = ByteConstants.FUNCTION_DIRECTION_MOTOR;

        byte postionNum;
        if (postion == POSTION_MAIN_DRIVER){
            postionNum = ByteConstants.Seat.SEAT_MAIN;
        }else{
            postionNum = ByteConstants.Seat.SEAT_VICE;
        }

        String hex = Integer.toHexString(progress);

        sendMainControlBytes[3] = function;
        sendMainControlBytes[4] = motorType;
        sendMainControlBytes[5] = Byte.parseByte(hex,16);
        sendMainControlBytes[6] = postionNum;

        mainViewModel.sendMsg(sendMainControlBytes);
    }

    /**
     * 颈部气囊调节
     * @param airbagGear
     * @param postion
     */
    public void sendNeckAirbag(byte airbagGear,int postion){
        sendAirbag(ByteConstants.Airbag.NECK_AIRBAG,airbagGear,postion);
    }

    /**
     * 腰部托气囊调节
     * @param airbagGear
     * @param postion
     */
    public void sendWaistEntrustAirbag(byte airbagGear,int postion){
        sendAirbag(ByteConstants.Airbag.WAIST_AIRBAG,airbagGear,postion);
    }

    /**
     * 腰部点气囊调节
     * @param progress
     * @param waistPointEnable
     * @param postion
     */
    public void sendWaistPointAirbag(int progress, boolean waistPointEnable, int postion){
        byte airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_CLOSE;;
        if (waistPointEnable){
            switch (progress){
                case 1:
                    airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_1;
                    break;
                case 2:
                    airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_2;
                    break;
                case 3:
                    airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_3;
                    break;
                case 4:
                    airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_4;
                    break;
                case 5:
                    airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_5;
                    break;
                default:
                    break;
            }
        }else{
            airbagGear = ByteConstants.Airbag.WAIST_POINT_AIRBAG_CLOSE;
        }

        sendAirbag(ByteConstants.Airbag.WAIST_POINT_AIRBAG,airbagGear,postion);
    }

    /**
     * 气囊调节
     * @param airbagType 气囊类型
     * @param airbagGear 档位
     * @param postion 位置
     */
    private void sendAirbag(byte airbagType,byte airbagGear,int postion){
        byte [] sendAirBagBytes = new byte[] {
            ByteConstants.FRAME_HEADER,
            ByteConstants.FRAME_HEADER2,
            ByteConstants.DEVICE_PNEUMATIC,
            0,
            0,
            0,
            0,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.TEMP_DATA,
            ByteConstants.FRAME_END,
            ByteConstants.FRAME_END2};

        byte function = ByteConstants.FUNCTION_MASSAGE;

        byte postionNum;
        if (postion == POSTION_MAIN_DRIVER){
            postionNum = ByteConstants.Seat.SEAT_MAIN;
        }else{
            postionNum = ByteConstants.Seat.SEAT_VICE;
        }
        sendAirBagBytes[3] = function;
        sendAirBagBytes[4] = airbagType;
        sendAirBagBytes[5] = airbagGear;
        sendAirBagBytes[6] = postionNum;
        mainViewModel.sendMsg(sendAirBagBytes);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void seatSetChange(SettingSeatInfoChange event) {
        //view.seatSetChange();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void voiceChange(VoiceChangeEvent event) {
        Log.d("zyp", ": receive voice change !");
        switch (event.getType()){
            case VoiceUpRecognitionManager.OPEN_VENTILATION:
                adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_VENTILATION,
                        1, ByteConstants.VentilationHeat.VENTILATION_HEAT_1);
                break;
            case VoiceUpRecognitionManager.CLOSE_VENTILATION:
                adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_VENTILATION,
                        1, ByteConstants.VentilationHeat.VENTILATION_HEAT_CLOSE);
                break;
            case VoiceUpRecognitionManager.OPEN_HEAT:
                adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_HEAT,
                        1, ByteConstants.VentilationHeat.VENTILATION_HEAT_1);
                break;
            case VoiceUpRecognitionManager.CLOSE_HEAT:
                adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_HEAT,
                        1, ByteConstants.VentilationHeat.VENTILATION_HEAT_CLOSE);
                break;
            case VoiceUpRecognitionManager.OPEN_LIGHT:
                adjustLight(1, ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_WHITE);
                break;
            case VoiceUpRecognitionManager.CLOSE_LIGHT:
                adjustLight(1, ByteConstants.RgbLightStrip.RGB_LIGHT_STRIP_CLOSE);
                break;
            default:
                break;
        }

        view.setVoiceChangeView(event.getType());
    }

    @Override public void destroy() {
        super.destroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

}
