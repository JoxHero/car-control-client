package com.wissci.carcontrol.view.ui.setting;

import com.wissci.carcontrol.MainViewModel;
import com.wissci.carcontrol.base.presenter.BasePresenter;

import javax.inject.Inject;
import lombok.Setter;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class SettingPresenter extends BasePresenter<SettingView> {
  @Setter private MainViewModel mainViewModel;

  @Inject public SettingPresenter() {
  }
}
