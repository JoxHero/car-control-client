package com.wissci.carcontrol.view.ui.location;

import com.wissci.carcontrol.R;
import com.wissci.carcontrol.base.mvp.BaseMvpFragment;

/**
 * @Description: 定位
 * Created by yongpengzhang on 2021/7/1
 */
public class LocationFragment extends BaseMvpFragment<LocationPresenter> implements LocationView {

    public static LocationFragment newInstance() {
        LocationFragment fragment = new LocationFragment();
        return fragment;
    }


    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    protected void setupView() {

    }
}