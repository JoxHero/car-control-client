package com.wissci.carcontrol.view.widget;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.content.ContextCompat;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.utils.ImageUtils;

/**
 * 环形 进度条
 *
 * @Description: Created by yongpengzhang on 2021/7/7
 */
public class ProgressImageView extends AppCompatImageView {
  private Context context;
  /**
   * 加载的进度
   */
  private int progress = 0;
  private int margin = 10;
  private Paint mPaint;
  private Paint progressBgPaint;

  private int totalProgress = 6;
  private String arcColor;
  private String defaultColor = "#A3622F";

  private ProgressChangeListener progressChangeListener;
  private boolean isDrawProgress = true;
  public ProgressImageView(Context context) {
    super(context);
    this.context = context;
    init();
  }

  public ProgressImageView(Context context, @Nullable AttributeSet attrs) {
    super(context, attrs);
    this.context = context;
    TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.ProgressImageView);
    totalProgress = array.getInteger(R.styleable.ProgressImageView_progressTotal, 6);
    arcColor = array.getString(R.styleable.ProgressImageView_progressArcColor);
    defaultColor = array.getString(R.styleable.ProgressImageView_defaultColor);
    isDrawProgress = array.getBoolean(R.styleable.ProgressImageView_isDrawProgress,true);

    if (defaultColor == null) {
      defaultColor = "#67452A";
    }
    array.recycle();
    init();
  }

  public ProgressImageView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    this.context = context;
    init();
  }

  private void init() {
    mPaint = new Paint();
    mPaint.setAntiAlias(true);
    mPaint.setStrokeWidth(5);
    mPaint.setColor(Color.parseColor(defaultColor));
    mPaint.setStyle(Paint.Style.STROKE);

    progressBgPaint = new Paint();
    progressBgPaint.setAntiAlias(true);
    progressBgPaint.setStrokeWidth(5);
    progressBgPaint.setColor(Color.WHITE);
    progressBgPaint.setStyle(Paint.Style.STROKE);
  }

  public void setProgressChangeListener(ProgressChangeListener progressChangeListener) {
    this.progressChangeListener = progressChangeListener;
  }

  @Override protected void onDraw(Canvas canvas) {
    super.onDraw(canvas);
    RectF rectF = new RectF();
    rectF.left = 0 + margin;
    rectF.top = 0 + margin;
    rectF.right = getMeasuredWidth() - margin;
    rectF.bottom = getMeasuredHeight() - margin;
    canvas.drawArc(rectF, 0, 360, false, progressBgPaint);

    if (isDrawProgress){
      canvas.drawArc(rectF, 270, progress, false, mPaint);
    }

  }

  public void setTotalProgress(int totalProgress) {
    this.totalProgress = totalProgress;
  }

  public void setArcColor(String arcColor) {
    this.arcColor = arcColor;
  }

  int currentProgress;
  public void nextStep() {
    currentProgress--;

    if (currentProgress == -1){
      currentProgress = totalProgress;
    }

    if (currentProgress == 0){
      mPaint.setColor(Color.parseColor(defaultColor));
      setImageDrawable(ImageUtils.tintDrawable(getDrawable(),
          ColorStateList.valueOf(Color.parseColor(defaultColor))));
    }else{
      mPaint.setColor(Color.parseColor(arcColor));
      setImageDrawable(ImageUtils.tintDrawable(getDrawable(),
          ColorStateList.valueOf(Color.parseColor(arcColor))));
    }

    this.progress = currentProgress * 360 / totalProgress;
    if (progressChangeListener != null){
      progressChangeListener.progressChange(this,progress);
    }
    postInvalidate();
  }

  public void clear(){
    this.progress = 0;
    currentProgress = 0;
    mPaint.setColor(Color.parseColor(defaultColor));
    setImageDrawable(ImageUtils.tintDrawable(getDrawable(),
        ColorStateList.valueOf(Color.parseColor(defaultColor))));
    postInvalidate();
  }

  public void setProgress(int progress){
    this.currentProgress = progress;
    this.progress = currentProgress * 360 / totalProgress;
    if (currentProgress == 0){
      mPaint.setColor(Color.parseColor(defaultColor));
      setImageDrawable(ImageUtils.tintDrawable(getDrawable(),
          ColorStateList.valueOf(Color.parseColor(defaultColor))));
    }else{
      mPaint.setColor(Color.parseColor(arcColor));
      setImageDrawable(ImageUtils.tintDrawable(getDrawable(),
          ColorStateList.valueOf(Color.parseColor(arcColor))));
    }
    postInvalidate();
  }

  public int getCurrentProgress() {
    return currentProgress;
  }

  public interface ProgressChangeListener{
    void progressChange(ProgressImageView view,int progress);
  }
}
