package com.wissci.carcontrol.view.ui.home;

import android.graphics.Color;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.lifecycle.ViewModelProvider;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.wissci.carcontrol.RxJava;
import com.wissci.carcontrol.utils.HandlerUtil;

import com.wissci.carcontrol.MainViewModel;
import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.base.mvp.BaseMvpFragment;
import com.wissci.carcontrol.utils.ByteConstants;
import com.wissci.carcontrol.view.widget.ProgressImageView;
import com.wissci.carcontrol.view.widget.SeekArc;
import com.wissci.carcontrol.view.widget.ShadowProperty;
import com.wissci.carcontrol.view.widget.ShadowViewDrawable;
import com.wissci.carcontrol.view.widget.VerticalSeekBar;
import com.wissci.carcontrol.vo.SettingSeatInfo;
import com.wissci.carcontrol.voice.VoiceUpRecognitionManager;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Predicate;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

/**
 * @Description: Created by yongpengzhang on 2021/7/1
 */
public class HomeFragment extends BaseMvpFragment<HomePresenter> implements HomeView {
    @Inject
    PadManager padManager;
    @BindView(R.id.view_main_driver)
    View mainDriverView;
    @BindView(R.id.view_vice_driver)
    View viceDriverView;

    private MainViewModel mainViewModel;
    private DriverPostionHolder mainDriverHolder;
    private DriverPostionHolder viceDriverHolder;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override
    protected void setupView() {
        mainViewModel = new ViewModelProvider(getActivity()).get(MainViewModel.class);
        presenter.setMainViewModel(mainViewModel);
        mainDriverHolder =
                new DriverPostionHolder(mainDriverView, DriverPostionHolder.POSTION_MAIN_DRIVER);
        viceDriverHolder =
                new DriverPostionHolder(viceDriverView, DriverPostionHolder.POSTION_VICE_DRIVER);
    }

    @Override
    public void onResume() {
        super.onResume();
        seatSetChange();
    }

    @Override
    public void seatSetChange() {
        if (mainDriverHolder != null) {
            mainDriverHolder.setMotorInfo();
        }

        if (viceDriverHolder != null) {
            HandlerUtil.runOnUiThreadDelay(() -> viceDriverHolder.setMotorInfo(), 1000);
        }
    }

    class DriverPostionHolder implements View.OnTouchListener {
        /**
         * 主驾驶位
         */
        public static final int POSTION_MAIN_DRIVER = 1;
        /**
         * 副驾驶位
         */
        public static final int POSTION_VICE_DRIVER = 2;

        /**
         * 驾驶位置
         */
        private int drivingPosition;

        @BindView(R.id.tv_seat_title)
        TextView tvSeatTitle;
        @BindView(R.id.iv_auto)
        ProgressImageView ivAuto;
        @BindView(R.id.iv_heat)
        ProgressImageView ivHeat;
        @BindView(R.id.iv_music)
        ProgressImageView ivMusic;
        @BindView(R.id.iv_ventilation)
        ProgressImageView ivVentilation;
        @BindView(R.id.iv_light)
        ProgressImageView ivLight;
        @BindView(R.id.seekarc_seat_back)
        SeekArc saSeatBack;
        @BindView(R.id.sb_vice_verical)
        VerticalSeekBar sbViceVerical;
        @BindView(R.id.sb_foot_rest)
        AppCompatSeekBar sbFootRest;
        @BindView(R.id.sb_seat_horizion)
        AppCompatSeekBar sbSeatHorizion;
        @BindView(R.id.tv_waistentrust_top)
        Button tvWaistentrustTop;
        @BindView(R.id.tv_waistentrust_bottom)
        Button tvWaistentrustBottom;
        @BindView(R.id.tv_waistentrust_left)
        Button tvWaistentrustLeft;
        @BindView(R.id.tv_waistentrust_right)
        Button tvWaistentrustRight;
        @BindView(R.id.tv_neck_chong)
        Button tvNeckChone;
        @BindView(R.id.tv_neck_fang)
        Button tvNeckFang;
        @BindView(R.id.cl_waistentrust)
        ConstraintLayout clWaistentrust;
        Disposable viceVericalDisposable = null;
        Disposable footDisposable = null;
        Disposable viceHorizionDisposable = null;
        Disposable viceBackDisposable = null;

        public DriverPostionHolder(View rootView, int drivingPosition) {
            ButterKnife.bind(this, rootView);
            this.drivingPosition = drivingPosition;
            switch (drivingPosition) {
                case POSTION_MAIN_DRIVER:
                    tvSeatTitle.setText(R.string.main_driver);
                    break;
                case POSTION_VICE_DRIVER:
                    tvSeatTitle.setText(R.string.vice_driver);
                    break;
                default:
                    break;
            }

            sbViceVerical.setSelectColor(ContextCompat.getColor(getActivity(),R.color.main_color));
            sbViceVerical.setUnSelectColor(Color.GRAY);
            sbFootRest.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_4, drivingPosition, seekBar.getProgress());

                    /*if (footDisposable == null){
                        footDisposable = RxJava.interval(500, aLong -> {
                            Log.d("zyp", "viceVerical onProgress: " + progress);
                            presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_4, drivingPosition, seekBar.getProgress());
                        });
                    }*/

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (footDisposable != null && !footDisposable.isDisposed()){
                        footDisposable.dispose();
                        footDisposable = null;
                    }
                }
            });

            sbSeatHorizion.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_1, drivingPosition, progress);

                    //if (viceHorizionDisposable == null){
                    //    viceHorizionDisposable = RxJava.interval(500, aLong -> {
                    //        Log.d("zyp", "viceHoriztion onProgress: " + progress);
                    //        presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_4, drivingPosition, progress);
                    //    });
                    //}
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (viceHorizionDisposable != null && !viceHorizionDisposable.isDisposed()){
                        viceHorizionDisposable.dispose();
                        viceHorizionDisposable = null;
                    }
                }
            });


            sbViceVerical.setOnSlideChangeListener(new VerticalSeekBar.SlideChangeListener() {
                @Override
                public void onStart(VerticalSeekBar slideView, int progress) {

                }

                @Override
                public void onProgress(VerticalSeekBar slideView, int progress) {
                    Log.d("zyp", " onProgress: " + progress);
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_2, drivingPosition, progress);
                    //if (viceVericalDisposable == null){
                    //    viceVericalDisposable = RxJava.interval(500, aLong -> {
                    //        Log.d("zyp", "viceVerical onProgress: " + progress);
                    //        presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_2, drivingPosition, progress);
                    //    });
                    //}
                }

                @Override
                public void onStop(VerticalSeekBar slideView, int progress) {
                    if (viceVericalDisposable != null && !viceVericalDisposable.isDisposed()){
                        viceVericalDisposable.dispose();
                        viceVericalDisposable = null;
                    }
                }
            });

            saSeatBack.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
                @Override
                public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_3, drivingPosition, progress);

                    if (viceBackDisposable == null){
                        //viceBackDisposable = RxJava.interval(500, aLong -> {
                        //    Log.d("zyp", "vice back onProgress: " + progress);
                        //    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_3, drivingPosition, progress);
                        //});
                    }
                }

                @Override
                public void onStartTrackingTouch(SeekArc seekArc) {

                }

                @Override
                public void onStopTrackingTouch(SeekArc seekArc) {
                    if (viceBackDisposable != null && !viceBackDisposable.isDisposed()){
                        viceBackDisposable.dispose();
                        viceBackDisposable = null;
                    }
                }
            });

            tvWaistentrustTop.setOnTouchListener(this::onTouch);
            tvWaistentrustBottom.setOnTouchListener(this::onTouch);
            tvWaistentrustLeft.setOnTouchListener(this::onTouch);
            tvWaistentrustRight.setOnTouchListener(this::onTouch);
            tvNeckChone.setOnTouchListener(this::onTouch);
            tvNeckFang.setOnTouchListener(this::onTouch);

           /* ShadowProperty sp = new ShadowProperty().setShadowColor(0x77000000)
                .setShadowDy(dp2px(1f))
                .setShadowRadius(dp2px(5))
                .setShadowSide(ShadowProperty.RIGHT | ShadowProperty.BOTTOM);
            ShadowViewDrawable sd = new ShadowViewDrawable(sp, Color.WHITE, 0, 0);
            ViewCompat.setBackground(clWaist, sd);
            ViewCompat.setLayerType(clWaist, ViewCompat.LAYER_TYPE_SOFTWARE, null);*/
        }

        public void setMotorInfo() {
            SettingSeatInfo info = padManager.getSettingSeatInfo(padManager.getCurrentSeatInfoSetPostion());
            if (info == null) {
                return;
            }
            if (drivingPosition == POSTION_MAIN_DRIVER) {
                sbFootRest.setProgress(info.getMainFootMotorPostion());
                sbSeatHorizion.setProgress(info.getMainBaMotorPostion());
                sbViceVerical.setProgress(info.getMainUdMotorPostion());
                saSeatBack.setProgress(info.getMainTiltMotorPostion());
            } else {
                sbFootRest.setProgress(info.getViceFootMotorPostion());
                sbSeatHorizion.setProgress(info.getViceBaMotorPostion());
                sbViceVerical.setProgress(info.getViceUdMotorPostion());
                saSeatBack.setProgress(info.getViceTiltMotorPostion());
            }

            HandlerUtil.runOnUiThreadDelay(new Runnable() {
                @Override
                public void run() {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_4, drivingPosition, sbFootRest.getProgress());
                }
            }, 200);

            HandlerUtil.runOnUiThreadDelay(new Runnable() {
                @Override
                public void run() {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_1, drivingPosition, sbSeatHorizion.getProgress());
                }
            }, 400);

            HandlerUtil.runOnUiThreadDelay(new Runnable() {
                @Override
                public void run() {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_2, drivingPosition, sbViceVerical.getProgress());
                }
            }, 600);

            HandlerUtil.runOnUiThreadDelay(new Runnable() {
                @Override
                public void run() {
                    presenter.sendDirectionMotor(ByteConstants.Motor.MOTOR_TYPE_3, drivingPosition, saSeatBack.getProgress());
                }
            }, 800);

        }

        int waistPointStatus = 1;
        boolean waistPointEnable = false;
        @OnClick({R.id.iv_auto, R.id.iv_heat, R.id.iv_music, R.id.iv_ventilation,
                R.id.iv_light, R.id.tv_neck_chong, R.id.tv_neck_fang,
                R.id.tv_waistentrust_top, R.id.tv_waistentrust_left, R.id.tv_waistentrust_bottom, R.id.tv_waistentrust_right, R.id.tv_waistentrust_center})
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_auto:
                    ivAuto.nextStep();
                    if (ivAuto.getCurrentProgress() != 0){
                        ivHeat.clear();
                        ivVentilation.clear();
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_HEAT, drivingPosition,
                                0);
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_VENTILATION,
                                drivingPosition, 0);
                    }
                    presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_AUTO, drivingPosition,
                            ivAuto.getCurrentProgress());
                    break;
                case R.id.iv_heat:
                    ivHeat.nextStep();
                    if (ivHeat.getCurrentProgress() != 0){
                        ivAuto.clear();
                        ivVentilation.clear();
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_AUTO, drivingPosition,
                                0);
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_VENTILATION, drivingPosition,
                                0);
                    }
                    presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_HEAT, drivingPosition,
                            ivHeat.getCurrentProgress());
                    break;
                case R.id.iv_music:
                    ivMusic.nextStep();
                    presenter.sendWaistPointAirbag(ivMusic.getCurrentProgress(), waistPointEnable,
                            drivingPosition);
                    break;
                case R.id.iv_ventilation:
                    ivVentilation.nextStep();
                    if (ivVentilation.getCurrentProgress() != 0){
                        ivAuto.clear();
                        ivHeat.clear();
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_AUTO, drivingPosition,
                                0);
                        presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_HEAT, drivingPosition,
                                0);
                    }
                    presenter.adjustVentilationHeat(ByteConstants.VentilationHeat.TYPE_VENTILATION,
                            drivingPosition, ivVentilation.getCurrentProgress());
                    break;
                case R.id.iv_light:
                    ivLight.nextStep();
                    presenter.adjustLight(drivingPosition, ivLight.getCurrentProgress());
                    break;
                /*case R.id.tv_neck_chong:
                    presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_INFLATE, drivingPosition);
                    break;
                case R.id.tv_neck_fang:
                    presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_DEFLATE, drivingPosition);
                    break;
                case R.id.tv_waistentrust_top:
                    break;
                case R.id.tv_waistentrust_left:
                    presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_4, drivingPosition);
                    break;
                case R.id.tv_waistentrust_right:
                    presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_3, drivingPosition);
                    break;
                case R.id.tv_waistentrust_bottom:
                    presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_2, drivingPosition);
                    break;*/
                case R.id.tv_waistentrust_center:
                    if (waistPointStatus > 5) {
                        waistPointStatus = 1;
                    }
                    waistPointEnable = !waistPointEnable;

                    if (!waistPointEnable){
                        clWaistentrust.setBackgroundResource(R.drawable.status1);
                    }else {
                        switch (waistPointStatus){
                            case 1:
                                clWaistentrust.setBackgroundResource(R.drawable.status2);
                                break;
                            case 2:
                                clWaistentrust.setBackgroundResource(R.drawable.status3);
                                break;
                            case 3:
                                clWaistentrust.setBackgroundResource(R.drawable.status4);
                                break;
                            case 4:
                                clWaistentrust.setBackgroundResource(R.drawable.status5);
                                break;
                            case 5:
                                clWaistentrust.setBackgroundResource(R.drawable.status6);
                                break;
                            default:
                                break;
                        }
                    }
                    presenter.sendWaistPointAirbag(waistPointStatus, waistPointEnable, drivingPosition);
                    if (waistPointEnable) {
                        waistPointStatus++;
                    }
                    break;
                default:
                    break;
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            Disposable disposable = null;
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    Log.d("zyp", "onTouch: DOWN");
                    /*disposable = RxJava.interval(500, aLong -> {
                        Log.d("zyp", "onTouch: DOWN222222222");
                        dispostWaistEntrustButton(true,v);
                    });*/
                    dispostWaistEntrustButton(true,v);
                    break;
                case MotionEvent.ACTION_UP:
                    Log.d("zyp", "onTouch: UP");
                    /*if (disposable != null && disposable.isDisposed()){
                        disposable.dispose();
                    }*/
                    dispostWaistEntrustButton(false,v);
                    break;
                default:
                    /*if (disposable != null && disposable.isDisposed()){
                        disposable.dispose();
                    }*/
                    break;
            }
            return false;
        }

        private void dispostWaistEntrustButton(boolean isDown,View view){
            switch (view.getId()){
                case R.id.tv_waistentrust_top:
                    if (isDown){
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_1, drivingPosition);

                    }else{
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_0, drivingPosition);
                    }
                    break;
                case R.id.tv_waistentrust_bottom:
                    if (isDown){
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_2, drivingPosition);
                    }else{
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_0, drivingPosition);
                    }
                    break;
                case R.id.tv_waistentrust_left:
                    if (isDown){
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_3, drivingPosition);
                    }else{
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_0, drivingPosition);
                    }
                    break;
                case R.id.tv_waistentrust_right:
                    if (isDown){
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_4, drivingPosition);
                    }else{
                        presenter.sendWaistEntrustAirbag(ByteConstants.Airbag.WAIST_AIRBAG_0, drivingPosition);
                    }
                    break;
                case R.id.tv_neck_chong:
                    if (isDown){
                        presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_INFLATE, drivingPosition);
                    }else{
                        presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_CLOSE, drivingPosition);
                    }
                    break;
                case R.id.tv_neck_fang:
                    if (isDown){
                        presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_DEFLATE, drivingPosition);
                    }else{
                        presenter.sendNeckAirbag(ByteConstants.Airbag.NECK_AIRBAG_GEAR_CLOSE, drivingPosition);
                    }
                    break;
                default:break;
            }
        }

        public void setVoiveChangeView(int type) {
            switch (type){
                case VoiceUpRecognitionManager.OPEN_VENTILATION:
                    ivVentilation.setProgress(1);
                    break;
                case VoiceUpRecognitionManager.CLOSE_VENTILATION:
                    ivVentilation.clear();
                    break;
                case VoiceUpRecognitionManager.OPEN_HEAT:
                    ivHeat.setProgress(1);
                    break;
                case VoiceUpRecognitionManager.CLOSE_HEAT:
                    ivHeat.clear();
                    break;
                case VoiceUpRecognitionManager.OPEN_LIGHT:
                    ivLight.setProgress(1);
                    break;
                case VoiceUpRecognitionManager.CLOSE_LIGHT:
                    ivLight.clear();
                    break;
                default:
                    break;
            }
        }
    }

    @Override public void setVoiceChangeView(int type) {
        mainDriverHolder.setVoiveChangeView(type);
        //viceDriverHolder.setVoiveChangeView(type);
    }

    private int dp2px(float dp) {
        final float scale = getActivity().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}