package com.wissci.carcontrol.view.ui.setting;

import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatSeekBar;
import androidx.lifecycle.ViewModelProvider;

import com.wissci.carcontrol.MainViewModel;
import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.base.mvp.BaseMvpFragment;
import com.wissci.carcontrol.common.event.SettingSeatInfoChange;
import com.wissci.carcontrol.common.utils.tool.ToastUtil;
import com.wissci.carcontrol.view.widget.SeekArc;
import com.wissci.carcontrol.view.widget.VerticalSeekBar;
import com.wissci.carcontrol.vo.SettingSeatInfo;

import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;

/**
 * @Description: 设置
 * Created by yongpengzhang on 2021/7/1
 */
public class SettingFragment extends BaseMvpFragment<SettingPresenter> implements SettingView {
    @BindView(R.id.view_main_driver)
    View mainDriverView;
    @BindView(R.id.view_vice_driver)
    View viceDriverView;
    private MainViewModel mainViewModel;
    @Inject
    PadManager padManager;

    public static SettingFragment newInstance() {
        SettingFragment fragment = new SettingFragment();
        return fragment;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_setting;
    }

    @Override
    protected void setupView() {
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        presenter.setMainViewModel(mainViewModel);

        DriverPostionHolder mainDriverHolder = new DriverPostionHolder(mainDriverView,
                DriverPostionHolder.POSTION_MAIN_DRIVER);
        DriverPostionHolder viceDriverHolder = new DriverPostionHolder(viceDriverView,
                DriverPostionHolder.POSTION_VICE_DRIVER);
    }

    class DriverPostionHolder {
        /**
         * 主驾驶位
         */
        public static final int POSTION_MAIN_DRIVER = 1;
        /**
         * 副驾驶位
         */
        public static final int POSTION_VICE_DRIVER = 2;

        /**
         * 驾驶位置
         */
        private int drivingPosition;

        @BindView(R.id.tv_seat_title)
        TextView tvSeatTitle;
        @BindView(R.id.seekarc_seat_back)
        SeekArc saSeatBack;
        @BindView(R.id.sb_vice_verical)
        VerticalSeekBar sbViceVerical;
        @BindView(R.id.sb_foot_rest)
        AppCompatSeekBar sbFootRest;
        @BindView(R.id.sb_seat_horizion)
        AppCompatSeekBar sbSeatHorizion;
        @BindView(R.id.cb_set_mode)
        CheckBox cbSetMode;
        @BindView(R.id.rg_mode)
        RadioGroup rgMode;
        @BindView(R.id.iv_set1) ImageView ivSet1;
        @BindView(R.id.iv_set2) ImageView ivSet2;
        @BindView(R.id.iv_set3) ImageView ivSet3;


        /**
         * 前后座椅调节进度
         */
        private int baMotorProgress;
        /**
         * 上下座椅调节进度
         */
        private int udMotorProgress;
        /**
         * 脚垫调节进度
         */
        private int footMotorProgress;
        /**
         * 靠背调节进度
         */
        private int titlMotorProgress;

        private SettingSeatInfo settingSeatInfo;

        public DriverPostionHolder(View rootView, int drivingPosition) {
            ButterKnife.bind(this, rootView);
            this.drivingPosition = drivingPosition;
            switch (drivingPosition) {
                case POSTION_MAIN_DRIVER:
                    tvSeatTitle.setText(R.string.main_driver);
                    break;
                case POSTION_VICE_DRIVER:
                    tvSeatTitle.setText(R.string.vice_driver);
                    break;
                default:
                    break;
            }
            setInfo();

            //setSbEnable(false);



            rgMode.check(padManager.getCurrentSeatInfoSetPostion());

            sbFootRest.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (!verifyModeCheck()) {
                        return;
                    }

                    footMotorProgress = seekBar.getProgress();
                }
            });

            sbSeatHorizion.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    if (!verifyModeCheck()) {
                        return;
                    }

                    baMotorProgress = seekBar.getProgress();
                }
            });

            sbViceVerical.setOnSlideChangeListener(new VerticalSeekBar.SlideChangeListener() {
                @Override
                public void onStart(VerticalSeekBar slideView, int progress) {

                }

                @Override
                public void onProgress(VerticalSeekBar slideView, int progress) {

                }

                @Override
                public void onStop(VerticalSeekBar slideView, int progress) {
                    if (!verifyModeCheck()) {
                        return;
                    }

                    udMotorProgress = progress;
                }
            });

            saSeatBack.setOnSeekArcChangeListener(new SeekArc.OnSeekArcChangeListener() {
                @Override
                public void onProgressChanged(SeekArc seekArc, int progress, boolean fromUser) {

                }

                @Override
                public void onStartTrackingTouch(SeekArc seekArc) {

                }

                @Override
                public void onStopTrackingTouch(SeekArc seekArc) {
                    if (!verifyModeCheck()) {
                        return;
                    }

                    titlMotorProgress = seekArc.getProgress();
                }
            });
        }

        private void setInfo(){
            SettingSeatInfo settingSeatInfo1 = padManager.getSettingSeatInfo(R.id.rb_mode1);
            SettingSeatInfo settingSeatInfo2 = padManager.getSettingSeatInfo(R.id.rb_mode2);
            SettingSeatInfo settingSeatInfo3 = padManager.getSettingSeatInfo(R.id.rb_mode3);
            switch (drivingPosition) {
                case POSTION_MAIN_DRIVER:
                    tvSeatTitle.setText(R.string.main_driver);
                    if (settingSeatInfo1 != null){
                        ivSet1.setSelected(settingSeatInfo1.isMainSet());
                    }else{
                        ivSet1.setSelected(false);
                    }

                    if (settingSeatInfo2 != null){
                        ivSet2.setSelected(settingSeatInfo2.isMainSet());
                    }else{
                        ivSet2.setSelected(false);
                    }

                    if (settingSeatInfo3 != null){
                        ivSet3.setSelected(settingSeatInfo3.isMainSet());
                    }else{
                        ivSet3.setSelected(false);
                    }
                    break;
                case POSTION_VICE_DRIVER:
                    tvSeatTitle.setText(R.string.vice_driver);
                    if (settingSeatInfo1 != null){
                        ivSet1.setSelected(settingSeatInfo1.isViceSet());
                    }else{
                        ivSet1.setSelected(false);
                    }

                    if (settingSeatInfo2 != null){
                        ivSet2.setSelected(settingSeatInfo2.isViceSet());
                    }else{
                        ivSet2.setSelected(false);
                    }

                    if (settingSeatInfo3 != null){
                        ivSet3.setSelected(settingSeatInfo3.isViceSet());
                    }else{
                        ivSet3.setSelected(false);
                    }
                    break;
                default:
                    break;
            }
        }

        @OnCheckedChanged({R.id.cb_set_mode, R.id.rb_mode1, R.id.rb_mode2, R.id.rb_mode3})
        public void onCheckedChangedEye(CompoundButton view, boolean isChecked) {
            switch (view.getId()) {
                case R.id.cb_set_mode:
                    //setSbEnable(isChecked);

                    //如果重置为未选中 则说明保存当前操作
                    if (!isChecked) {
                        if (rgMode.getCheckedRadioButtonId() == View.NO_ID) {
                            return;
                        }
                        setSeatInfo();
                        rgMode.clearCheck();
                    }
                    break;
                case R.id.rb_mode1:
                    rbChange(isChecked,R.id.rb_mode1);
                    break;
                case R.id.rb_mode2:
                    rbChange(isChecked,R.id.rb_mode2);
                    break;
                case R.id.rb_mode3:
                    rbChange(isChecked,R.id.rb_mode3);
                    break;
                default:
                    break;
            }
        }

        private void setSbEnable(boolean isChecked) {
            sbFootRest.setEnabled(isChecked);
            sbSeatHorizion.setEnabled(isChecked);
            sbViceVerical.setEnabled(isChecked);
            saSeatBack.setEnabled(isChecked);

            sbFootRest.setClickable(isChecked);
            sbSeatHorizion.setClickable(isChecked);
            sbViceVerical.setClickable(isChecked);
            saSeatBack.setClickable(isChecked);

            sbFootRest.setFocusable(isChecked);
            sbSeatHorizion.setFocusable(isChecked);
            sbViceVerical.setFocusable(isChecked);
            saSeatBack.setFocusable(isChecked);

            sbFootRest.setSelected(isChecked);
            sbSeatHorizion.setSelected(isChecked);
            sbViceVerical.setSelected(isChecked);
            saSeatBack.setSelected(isChecked);
        }

        private void rbChange(boolean isChecked,int postion) {
            if (!isChecked){
                return;
            }
            SettingSeatInfo info = padManager.getSettingSeatInfo(postion);
            if (info == null){
                sbFootRest.setProgress(0);
                sbSeatHorizion.setProgress(0);
                sbViceVerical.setProgress(0);
                saSeatBack.setProgress(0);
                return;
            }
            if (drivingPosition == POSTION_MAIN_DRIVER){
                sbFootRest.setProgress(info.getMainFootMotorPostion());
                sbSeatHorizion.setProgress(info.getMainBaMotorPostion());
                sbViceVerical.setProgress(info.getMainUdMotorPostion());
                saSeatBack.setProgress(info.getMainTiltMotorPostion());
            }else{
                sbFootRest.setProgress(info.getViceFootMotorPostion());
                sbSeatHorizion.setProgress(info.getViceBaMotorPostion());
                sbViceVerical.setProgress(info.getViceUdMotorPostion());
                saSeatBack.setProgress(info.getViceTiltMotorPostion());
            }

            //M键未选中 切换模式
            if (!cbSetMode.isChecked() && isChecked){
                padManager.setCurrentSeatInfoSetPostion(postion);
                EventBus.getDefault().post(new SettingSeatInfoChange());
            }
        }

        private boolean verifyModeCheck() {
            if (cbSetMode.isChecked()) {
                return true;
            }
            ToastUtil.show(getActivity(), getString(R.string.please_check_mode));
            return false;
        }

        private void setSeatInfo() {
            switch (rgMode.getCheckedRadioButtonId()) {
                case R.id.rb_mode1:
                    setSeatInfoContent(R.id.rb_mode1);
                    break;
                case R.id.rb_mode2:
                    setSeatInfoContent(R.id.rb_mode2);
                    break;
                case R.id.rb_mode3:
                    setSeatInfoContent(R.id.rb_mode3);
                    break;
                default:
                    break;
            }
        }

        private void setSeatInfoContent(int postion) {
            settingSeatInfo = padManager.getSettingSeatInfo(postion);
            if (settingSeatInfo == null) {
                settingSeatInfo = new SettingSeatInfo();
            }
            if (drivingPosition == POSTION_MAIN_DRIVER){
                settingSeatInfo.setMainTiltMotorPostion(titlMotorProgress != 0 ? titlMotorProgress : settingSeatInfo.getMainTiltMotorPostion());
                settingSeatInfo.setMainUdMotorPostion(udMotorProgress != 0 ? udMotorProgress : settingSeatInfo.getMainUdMotorPostion());
                settingSeatInfo.setMainBaMotorPostion(baMotorProgress != 0 ? baMotorProgress : settingSeatInfo.getMainBaMotorPostion());
                settingSeatInfo.setMainFootMotorPostion(footMotorProgress != 0 ? footMotorProgress : settingSeatInfo.getMainFootMotorPostion());
            }else{
                settingSeatInfo.setViceTiltMotorPostion(titlMotorProgress != 0 ? titlMotorProgress : settingSeatInfo.getViceTiltMotorPostion());
                settingSeatInfo.setViceUdMotorPostion(udMotorProgress != 0 ? udMotorProgress : settingSeatInfo.getViceUdMotorPostion());
                settingSeatInfo.setViceBaMotorPostion(baMotorProgress != 0 ? baMotorProgress : settingSeatInfo.getViceBaMotorPostion());
                settingSeatInfo.setViceFootMotorPostion(footMotorProgress != 0 ? footMotorProgress : settingSeatInfo.getViceFootMotorPostion());
            }

            padManager.setSettingSeatInfo(postion, settingSeatInfo);
            setInfo();
        }
    }
}