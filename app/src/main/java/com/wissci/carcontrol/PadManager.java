package com.wissci.carcontrol;

import android.content.Context;
import android.text.TextUtils;

import com.google.gson.reflect.TypeToken;
import com.wissci.carcontrol.common.utils.tool.SharedPreferencesUtils;
import com.wissci.carcontrol.utils.GsonUtils;
import com.wissci.carcontrol.utils.SPUtils;
import com.wissci.carcontrol.vo.SettingSeatInfo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;

/**
 * @Description: Created by yongpengzhang on 2021/8/13
 */
public class PadManager {
  private static final String MOTOR_SET_INFO = "MOTOR_SET_INFO";
  private static final String MOTOR_SET_POSTION = "MOTOR_SET_POSTION";

  /**
   * 主驾是否有人
   */
  @Setter @Getter
  public  boolean isMainPadHavePeople = false;
  /**
   * 副驾是否有人
   */
  @Setter @Getter
  public  boolean isVicePadHavePeople = false;
  /**
   * 主驾是否系安全带
   */
  @Setter @Getter
  public  boolean isMainSeatBelt = false;
  /**
   * 副驾是否系安全带
   */
  @Setter @Getter
  public  boolean isViceSeatBelt = false;
  /**
   * 当前车速
   */
  @Setter @Getter
  private volatile float speed;
  /**
   * 座椅设置信息
   */
  private Map<Integer,SettingSeatInfo> settingSeatInfoMap = new HashMap<>();
  /**
   * 当前选择的座椅记忆
   */

  private int currentSeatInfoSetPostion;


  @Inject
  public PadManager(Context context) {

  }

  public SettingSeatInfo getSettingSeatInfo(int postion){
    String seatSetInfoStr = SPUtils.getString(MOTOR_SET_INFO,null);
    if (!TextUtils.isEmpty(seatSetInfoStr)){
      settingSeatInfoMap = GsonUtils.fromJson(seatSetInfoStr
              , new TypeToken<Map<Integer,SettingSeatInfo>>() {}.getType());
    }
    if (settingSeatInfoMap == null){
      return null;
    }
    if (!settingSeatInfoMap.containsKey(postion)){
      return null;
    }
    return settingSeatInfoMap.get(postion);
  }

  public void setSettingSeatInfo(int postion,SettingSeatInfo settingSeatInfo) {
    this.settingSeatInfoMap.put(postion,settingSeatInfo);
    SPUtils.commitString(MOTOR_SET_INFO,GsonUtils.toJson(settingSeatInfoMap));
  }

  public int getCurrentSeatInfoSetPostion(){
    currentSeatInfoSetPostion = SPUtils.getInt(MOTOR_SET_POSTION,0);
    return currentSeatInfoSetPostion;
  }

  public SettingSeatInfo getCurrentSeatInfo() {
    currentSeatInfoSetPostion = SPUtils.getInt(MOTOR_SET_POSTION,0);
    if (settingSeatInfoMap == null){
      return null;
    }
    if (!settingSeatInfoMap.containsKey(currentSeatInfoSetPostion)){
      return null;
    }
    return settingSeatInfoMap.get(currentSeatInfoSetPostion);
  }

  public void setCurrentSeatInfoSetPostion(int currentSeatInfoSetPostion) {
    this.currentSeatInfoSetPostion = currentSeatInfoSetPostion;
    SPUtils.commitInt(MOTOR_SET_POSTION,currentSeatInfoSetPostion);
  }
}
