package com.wissci.carcontrol.base;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.viewholder.BaseViewHolder;
import com.wissci.carcontrol.R;

import java.util.List;

/**
 * @Description: RecycleView Adapter 基类
 * @Author: yongpeng.zhang@ixiandou.com.
 * @Time:   2019-09-12
 */
public abstract class BaseAdapter<T,K extends BaseViewHolder> extends BaseQuickAdapter<T, K > {
    public BaseAdapter(int layoutResId, @Nullable List<T> data) {
        super(layoutResId, data);
    }

    public BaseAdapter(int layoutResId) {
        super(layoutResId);
    }

    public void setEmptyView(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.view_empty, null);
        setEmptyView(view);
    }

    public void setEmptyView(Context context,String text){
        View view = LayoutInflater.from(context).inflate(R.layout.view_empty, null,true);
        TextView tvEmpty = view.findViewById(R.id.tv_empty);
        tvEmpty.setText(text);
        setEmptyView(view);
    }
}
