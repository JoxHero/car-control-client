package com.wissci.carcontrol.base;


import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.LayoutRes;
import androidx.annotation.Nullable;

import com.wissci.carcontrol.R;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.di.Injectable;
import com.wissci.carcontrol.event.NetWorkEvent;
import com.wissci.carcontrol.event.utils.EventBusUtils;
import com.wissci.carcontrol.navigation.NavigationController;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public abstract class BaseFragment extends DaggerFragment implements Injectable {
    private Unbinder unbinder;
    @Inject
    protected NavigationController navigationController;

    @LayoutRes
    protected abstract int getLayoutRes();

    protected abstract void setupView();

    @Target(ElementType.TYPE)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface BindEventBus {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (this.getClass().isAnnotationPresent(BindEventBus.class)) {
            if (EventBusUtils.isRegister(this)) {
                Log.e(Constants.APP_TAG, "the subscriber aleady registered !  ");
                return;
            }
            EventBusUtils.register(this);
        }
        setupView();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    @Subscribe
    public void netChangeEvent(NetWorkEvent event) {
        if (!event.isConnected()) {
            //showToastMessage(getString(R.string.no_network_tip));
        }
    }

    @Override
    public void onDestroy() {
        if (this.getClass().isAnnotationPresent(BindEventBus.class)) {
            if (EventBusUtils.isRegister(this)) {
                EventBusUtils.unregister(this);
            }
        }
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
