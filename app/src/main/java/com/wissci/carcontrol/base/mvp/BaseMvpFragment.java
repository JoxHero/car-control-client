package com.wissci.carcontrol.base.mvp;

import android.os.Bundle;

import androidx.annotation.Nullable;

import com.wissci.carcontrol.base.BaseFragment;
import com.wissci.carcontrol.base.presenter.BasePresenter;
import com.wissci.carcontrol.base.presenter.ViewImpl;

import javax.inject.Inject;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public abstract class BaseMvpFragment <T extends BasePresenter> extends BaseFragment implements ViewImpl {
    @Inject
    protected T presenter;

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        presenter.setView(this);
    }

    @Override
    public void onDestroyView() {
        presenter.destroy();
        super.onDestroyView();
    }
}
