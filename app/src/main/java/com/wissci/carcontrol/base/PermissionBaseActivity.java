package com.wissci.carcontrol.base;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.PermissionChecker;
import androidx.legacy.app.ActivityCompat;

import com.wissci.carcontrol.R;
import com.wissci.carcontrol.common.utils.tool.Util;

import dagger.android.support.DaggerAppCompatActivity;

/**
 * @Author: yongpeng.zhang@ixiandou.com.
 * @Time:   2019-09-29.
 */
public class PermissionBaseActivity extends DaggerAppCompatActivity {
    private final static int MY_PERMISSIONS_REQUEST = 1001;
    private final static int REQUEST_PERMISSION = 3;
    private String[] permissions;


    public CheckPermissionListener checkPermissionListener;

    public interface CheckPermissionListener {
        void hasPermission();

        void noPermission();
    }

    protected void checkPermission(final CheckPermissionListener checkPermissionListener,
                                   final String... permissions) {
        boolean isPermission = true;
        this.permissions = permissions;
        this.checkPermissionListener = checkPermissionListener;
        for (String permission : permissions) {
            if (PermissionChecker.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                isPermission = false;
            }
        }

        if (!isPermission) {
            askPermission(permissions);
            return;
        } else {
            checkPermissionListener.hasPermission();
        }
    }

    private void askPermission(final String... permissions) {
        ActivityCompat.requestPermissions(this, permissions,
                MY_PERMISSIONS_REQUEST);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST:
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        //ToastUtil.show(this, getString(R.string.please_set_permissions));
                        checkPermissionListener.noPermission();
                        return;
                    }
                }
                checkPermissionListener.hasPermission();
                break;
            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_PERMISSION:
                checkPermission(checkPermissionListener,permissions);
                break;
            default:
                break;
        }
    }

    protected void showNoPermissionDialog(){
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        dialog.setMessage(getString(R.string.please_set_permissions));
        dialog.setButton(AlertDialog.BUTTON_POSITIVE, "确定",
                (d, which) -> {
                    Intent intent = new Intent();
                    if (Util.getSDK_Version() > 23) {
                        intent.setAction(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
                    } else {
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                    }
                    Uri uri = Uri.fromParts("package", getPackageName(), null);
                    intent.setData(uri);
                    startActivityForResult(intent, REQUEST_PERMISSION);
                });
        dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "取消", (d, which) -> {
            dialog.dismiss();
            finish();
        });
        dialog.show();
    }
}
