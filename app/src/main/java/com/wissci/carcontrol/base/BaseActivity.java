package com.wissci.carcontrol.base;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.wissci.carcontrol.R;

import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.event.MessageEvent;
import com.wissci.carcontrol.event.NetWorkEvent;
import com.wissci.carcontrol.navigation.NavigationController;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public abstract class BaseActivity extends PermissionBaseActivity {
    protected final String TAG = getClass().getSimpleName();
    @Inject
    protected NavigationController navigation;
    @Inject
    protected Context context;
    @Nullable
    @BindView(R.id.toolbar_back)
    public ImageButton toolBarBack;
    @Nullable
    @BindView(R.id.toolbar_title)
    public TextView toolBarTitle;

    protected boolean isBack = true;

    private Unbinder unBinder;

    protected abstract int getLayoutRes();

    protected abstract void setupView();

    protected abstract void initialize();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutRes());
        if (Build.VERSION.SDK_INT != Build.VERSION_CODES.O) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
        unBinder = ButterKnife.bind(this);
        setupView();
        initialize();
        if (toolBarBack
                != null && isBack) {
            toolBarBack.setOnClickListener(v -> finish());
        }
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();
        unBinder = ButterKnife.bind(this);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        //MobclickAgent.onResume(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //MobclickAgent.onPause(this);
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unBinder.unbind();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {/* Do something */}

    @Subscribe
    public void netChangeEvent(NetWorkEvent event) {
        /*//设备没网 或者 没有连接wifi热点 直接跳转到系统网络连接页面
        if (!Constants.NetWorkState.isOnline || Constants.NetWorkState.NETWORK_TYPE == Constants.NetWorkState.WIFI_TYPE){
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
            return;
        }*/


    }
}
