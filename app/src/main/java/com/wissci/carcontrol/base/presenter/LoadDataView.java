/**
 * Copyright (C) 2014 android10.org. All rights reserved.
 *
 * @author Fernando Cejas (the android10 coder)
 */
package com.wissci.carcontrol.base.presenter;

import android.content.Context;

/**
 * Interface representing a View that will use to load com.zlw.tradeking.data.
 */
public interface LoadDataView extends ViewImpl {
   /* *//**
     * Show a view with a retryView bar indicating a loading process.
     *//*
    void showLoading();

    *//**
     * Hide a loading view.
     *//*
    void hideLoading();

    *//**
     * Show a retry view in case of an error when retrieving data.
     *//*
    void showRetry(String message);

    void hideRetry();
    
    void reset();

    *//**
     * Show an error message
     *
     * @param message A string representing an error.
     *//*
    void showError(String message);*/

    /**
     * Get a {@link Context}.
     */
    Context getContext();
}
