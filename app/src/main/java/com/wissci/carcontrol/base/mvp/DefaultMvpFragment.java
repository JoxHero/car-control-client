package com.wissci.carcontrol.base.mvp;

import android.os.Bundle;

import com.wissci.carcontrol.base.presenter.BasePresenter;


/**
 * Created by zyp on 2017/7/26.
 */

public abstract class DefaultMvpFragment<T extends BasePresenter> extends BaseMvpFragment<T> {
    protected abstract void initialize();

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initialize();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    public void onPause() {
        presenter.pause();
        super.onPause();
    }
}
