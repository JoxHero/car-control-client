package com.wissci.carcontrol.base.mvp;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.jaeger.library.StatusBarUtil;
import com.wissci.carcontrol.R;
import com.wissci.carcontrol.base.BaseActivity;
import com.wissci.carcontrol.base.presenter.BasePresenter;
import com.wissci.carcontrol.base.presenter.LoadDataView;
import com.wissci.carcontrol.base.presenter.ViewImpl;

import javax.inject.Inject;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public abstract class BaseMvpActivity <T extends BasePresenter>extends BaseActivity implements LoadDataView {
    @Inject
    protected T presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StatusBarUtil.setColor(this,getResources().getColor(R.color.main_color),0);
        presenter.setView(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.resume();
    }

    @Override
    protected void onPause() {
        presenter.pause();
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }


    @Override
    public Context getContext() {
        return context;
    }
}
