package com.wissci.carcontrol.base.presenter;

import org.greenrobot.eventbus.EventBus;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class BasePresenter<T extends ViewImpl> implements Presenter {
    
    protected T view;

    private volatile CompositeDisposable disposables = new CompositeDisposable();

    public BasePresenter() {
    }

    public void setView(T view) {
        this.view = view;
    }

    public void addDisposable(Disposable subscription) {
        validateDisposables();
        disposables.add(subscription);
    }

    public void validateDisposables() {
        if (disposables == null || disposables.isDisposed()) {
            disposables = new CompositeDisposable();
        }
    }

    public void cancel() {
        if (disposables != null && !disposables.isDisposed()) {
            disposables.dispose();
            disposables = null;
        }
    }
    
    @Override
    public void resume() {
    }

    @Override
    public void pause() {

//        cancel();
    }

    @Override
    public void retry() {

    }

    @Override
    public void destroy() {
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
        cancel();
    }

}
