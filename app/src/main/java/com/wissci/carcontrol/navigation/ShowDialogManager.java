package com.wissci.carcontrol.navigation;

import android.content.Context;
import android.view.View;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wissci.carcontrol.R;

import java.util.Calendar;

/**
 * @Author: zyp
 * @Time: 2019-06-12
 */
public class ShowDialogManager {

    /**
     * 基础 MateriaDialog
     */
    public static MaterialDialog showBasicDialog(Context context,
        String title,
        String content,
        String positiveText,
        String negativeText,
        MaterialDialog.SingleButtonCallback negativeCallback,
        MaterialDialog.SingleButtonCallback positiveCallback,
        boolean isCancel) {
        positiveText = positiveText == null ? context.getString(R.string.confirm) : positiveText;
        negativeText = negativeText == null ? context.getString(R.string.cancle) : negativeText;
        return new MaterialDialog.Builder(context)
            .title(title)
            .content(content)
            .positiveText(positiveText)
            .negativeText(negativeText)
            .onNegative(negativeCallback)
            .onPositive(positiveCallback)
            .cancelable(isCancel)
            .show();
    }


    public static MaterialDialog showOnlyConfirmDialog(Context context,
                                                 String title,
                                                 String content,
                                                 MaterialDialog.SingleButtonCallback positiveCallback,
                                                 boolean isCancel) {
        return showBasicDialog(context,title,content,context.getString(R.string.confirm),"",null,positiveCallback,isCancel);
    }

    public static void showInputBasicDialog(Context context,
                                            String title,
                                            String negativeText,
                                            String positiveText,
                                            String input,
                                            MaterialDialog.SingleButtonCallback singleButtonCallback,
                                            MaterialDialog.InputCallback inputCallback,
                                            boolean isCancel) {
        new MaterialDialog.Builder(context)
                .title(title)
                .negativeText(negativeText)
                .positiveText(positiveText)
                .onNegative(singleButtonCallback)
                .input(input, null, inputCallback)
                .cancelable(isCancel)
                .show();
    }

    public static void showInputDialog(Context context,
                                       String title,
                                       String input,
                                       MaterialDialog.InputCallback inputCallback,
                                       boolean isCancel) {
        showInputBasicDialog(context, title, "取消", "确定", input, null, inputCallback, isCancel);
    }


    public static void showSingleChooseDialog(Context context,
                                              String title,
                                              int selectIndex,
                                              MaterialDialog.ListCallbackSingleChoice singleCallBack,
                                              String... items) {
        new MaterialDialog.Builder(context)
                .title(title)
                .negativeText("取消")
                .items(items)
                .itemsCallbackSingleChoice(selectIndex, singleCallBack)
                .cancelable(true)
                .show();
    }

    public static void showSingleChooseDialog(Context context,
                                              String title,
                                              int selectIndex,
                                              MaterialDialog.SingleButtonCallback singleCallBack,
                                              String... items) {
        new MaterialDialog.Builder(context)
                .title(title)
                .negativeText("取消")
                .positiveText("确定")
                .items(items)
                .itemsCallbackSingleChoice(selectIndex, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View itemView, int which, CharSequence text) {
                        return false;
                    }
                })
                .onPositive(singleCallBack)
                .cancelable(true)
                .show();
    }

    public static void showInfoMessageDialog(Context context,
                                             String title,
                                             String content) {
        showBasicDialog(context, title, content, "确定", "", null, null, true);
    }

    public static void showCustomViewDialog(Context context,
                                            View view,
                                            String title,
                                            MaterialDialog.SingleButtonCallback positiveCallback) {
        new MaterialDialog.Builder(context)
                .title(title)
                .positiveText("确定")
                .negativeText("取消")
                .onPositive(positiveCallback)
                .titleGravity(GravityEnum.CENTER)
                .customView(view, true)
                .cancelable(true)
                .show();
    }

    public static void showCustomViewDialogNoAutoDismiss(Context context,
                                                         View view,
                                                         String title,
                                                         MaterialDialog.SingleButtonCallback positiveCallback) {
        new MaterialDialog.Builder(context)
                .title(title)
                .positiveText("确定")
                .negativeText("取消")
                .onPositive(positiveCallback)
                .onNegative((dialog, which) -> dialog.cancel())
                .titleGravity(GravityEnum.CENTER)
                .customView(view, true)
                .cancelable(true)
                .autoDismiss(false)
                .show();
    }

    /**
     * 日期选择弹窗
     *
     * @param fragmentManager
     * @param callBack
     */
    public static void showDatePickerDialog(FragmentManager fragmentManager, DatePickerDialog.OnDateSetListener callBack) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dialog = DatePickerDialog.newInstance(
                callBack,
                now);
        dialog.show(fragmentManager, "date_picker");

    }

    /**
     * 日期选择弹窗
     *
     * @param fragmentManager
     * @param callBack
     */
    public static void showDatePickerDialog2(FragmentManager fragmentManager, DatePickerDialog.OnDateSetListener callBack) {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dialog = DatePickerDialog.newInstance(
                callBack,
                now.get(Calendar.YEAR), now.get(Calendar.MONTH), 0);
        dialog.show(fragmentManager, "date_picker");

    }

    /**
     * 时间选择弹窗
     *
     * @param fragmentManager
     * @param callBack
     */
    public static void showTimePickerDialog(FragmentManager fragmentManager, TimePickerDialog.OnTimeSetListener callBack) {
        Calendar now = Calendar.getInstance();
        TimePickerDialog.newInstance(
                callBack,
                now.get(Calendar.HOUR_OF_DAY),
                now.get(Calendar.MINUTE),
                false).show(fragmentManager, "time_picker");
    }

    public static void showHintInfoMessageDialog(Context context,
                                                 String content, MaterialDialog.SingleButtonCallback positiveCallback) {
        showBasicDialog(context, "提示", content, "确定", "取消", null, positiveCallback, true);
    }

    public static void showHintInfoTitleMessageDialog(Context context, String title,
                                                      String content, MaterialDialog.SingleButtonCallback positiveCallback) {
        showBasicDialog(context, title, content, "确定", "取消", null, positiveCallback, true);
    }

    public static void showHintInfoConfirmMessageDialog(Context context, String title,
                                                        String content, MaterialDialog.SingleButtonCallback positiveCallback) {
        showBasicDialog(context, title, content, "确定", "", null, positiveCallback, false);
    }

    public static void showMemberAuthorityDialog(Context context, String title,
                                                 String content,
                                                 MaterialDialog.SingleButtonCallback negativeCallback,
                                                 MaterialDialog.SingleButtonCallback positiveCallback) {
        showBasicDialog(context, title, content, "去开通", "取消", negativeCallback, positiveCallback, true);
    }

    public static MaterialDialog showHintInfoConfirmNoCancelMessageDialog(Context context, String title,
                                                                          String content, MaterialDialog.SingleButtonCallback positiveCallback) {
        return new MaterialDialog.Builder(context)
                .title(title)
                .content(content)
                .positiveText("确定")
                .negativeText("")
                .onNegative(null)
                .onPositive(positiveCallback)
                .cancelable(false)
                .build();
    }
}
