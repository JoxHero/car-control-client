package com.wissci.carcontrol.navigation;

import android.app.Activity;
import android.content.Intent;

import com.wissci.carcontrol.MainActivity;
import com.wissci.carcontrol.common.utils.tool.SharedPreferencesUtils;


/**
 * @Description: 页面跳转控制器
 * @Author: zyp
 * @Time: 2019-06-13
 */
public class NavigationController {
    private SharedPreferencesUtils sharedPreferencesUtils;

    public NavigationController(
                                SharedPreferencesUtils sharedPreferencesUtils) {
        this.sharedPreferencesUtils = sharedPreferencesUtils;
    }



    public void toMain(Activity context) {
        //Intent intent = MainActivity.getCallingIntent(context, MainActivity.FROM_FLAG_NO_PLACE);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //context.startActivity(intent);
    }

}
