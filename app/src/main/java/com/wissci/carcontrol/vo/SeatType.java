package com.wissci.carcontrol.vo;

public enum SeatType {
    /**
     * 座椅前后
     */
    BAMOTORPOSTION,
    /**
     * 座椅上下
     */
    UDMOTORPOSTION,
    /**
     * 座椅倾斜
     */
    TILTMOTORPOSTION,
    /**
     * 脚垫
     */
    FOOTMOTORPOSTION
}
