package com.wissci.carcontrol.vo;

/**
 * 座椅设置信息
 */
public class SettingSeatInfo {
    /**
     * 主驾座椅前后位置
     */
    private int mainBaMotorPostion;
    /**
     * 主驾座椅上下位置
     */
    private int mainUdMotorPostion;
    /**
     * 主驾座椅倾斜位置
     */
    private int mainTiltMotorPostion;
    /**
     * 主驾脚托上下位置
     */
    private int mainFootMotorPostion;

    /**
     * 副驾座椅前后位置
     */
    private int viceBaMotorPostion;
    /**
     * 副驾座椅上下位置
     */
    private int viceUdMotorPostion;
    /**
     * 副驾座椅倾斜位置
     */
    private int viceTiltMotorPostion;
    /**
     * 副驾脚托上下位置
     */
    private int viceFootMotorPostion;

    public int getMainBaMotorPostion() {
        return mainBaMotorPostion;
    }

    public void setMainBaMotorPostion(int mainBaMotorPostion) {
        this.mainBaMotorPostion = mainBaMotorPostion;
    }

    public int getMainUdMotorPostion() {
        return mainUdMotorPostion;
    }

    public void setMainUdMotorPostion(int mainUdMotorPostion) {
        this.mainUdMotorPostion = mainUdMotorPostion;
    }

    public int getMainTiltMotorPostion() {
        return mainTiltMotorPostion;
    }

    public void setMainTiltMotorPostion(int mainTiltMotorPostion) {
        this.mainTiltMotorPostion = mainTiltMotorPostion;
    }

    public int getMainFootMotorPostion() {
        return mainFootMotorPostion;
    }

    public void setMainFootMotorPostion(int mainFootMotorPostion) {
        this.mainFootMotorPostion = mainFootMotorPostion;
    }

    public int getViceBaMotorPostion() {
        return viceBaMotorPostion;
    }

    public void setViceBaMotorPostion(int viceBaMotorPostion) {
        this.viceBaMotorPostion = viceBaMotorPostion;
    }

    public int getViceUdMotorPostion() {
        return viceUdMotorPostion;
    }

    public void setViceUdMotorPostion(int viceUdMotorPostion) {
        this.viceUdMotorPostion = viceUdMotorPostion;
    }

    public int getViceTiltMotorPostion() {
        return viceTiltMotorPostion;
    }

    public void setViceTiltMotorPostion(int viceTiltMotorPostion) {
        this.viceTiltMotorPostion = viceTiltMotorPostion;
    }

    public int getViceFootMotorPostion() {
        return viceFootMotorPostion;
    }

    public void setViceFootMotorPostion(int viceFootMotorPostion) {
        this.viceFootMotorPostion = viceFootMotorPostion;
    }

    public boolean isMainSet(){
        if (mainBaMotorPostion == 0 && mainFootMotorPostion ==0 && mainTiltMotorPostion ==0 &&mainUdMotorPostion == 0){
            return false;
        }
        return true;
    }

    public boolean isViceSet(){
        if (viceBaMotorPostion == 0 && viceFootMotorPostion ==0 && viceTiltMotorPostion ==0 && viceUdMotorPostion == 0){
            return false;
        }
        return true;
    }
}
