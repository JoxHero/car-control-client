package com.wissci.carcontrol.event;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class SpeedEvent {
    private float speed;
}
