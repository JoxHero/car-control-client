package com.wissci.carcontrol.event;

/**
 * @Description: Created by yongpengzhang on 2021/9/22
 */
public class VoiceChangeEvent {
    private int type;

    public VoiceChangeEvent(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
