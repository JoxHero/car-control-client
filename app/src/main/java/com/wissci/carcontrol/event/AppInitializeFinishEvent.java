package com.wissci.carcontrol.event;

import lombok.AllArgsConstructor;
import lombok.Data;
/**
 * Created by yongpengzhang on 2021/7/2
 */
@Data
@AllArgsConstructor
public class AppInitializeFinishEvent {
}
