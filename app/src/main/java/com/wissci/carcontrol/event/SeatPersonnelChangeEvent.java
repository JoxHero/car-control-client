package com.wissci.carcontrol.event;

public class SeatPersonnelChangeEvent {
  private byte[] receiveData;
  public SeatPersonnelChangeEvent(byte[] receiveData) {
    this.receiveData = receiveData;
  }

  public byte[] getReceiveData() {
    return receiveData;
  }
}
