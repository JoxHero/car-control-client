package com.wissci.carcontrol.event;

/**
 * Created by yongpengzhang on 2021/7/2
 */
public class NetWorkEvent {
    private boolean isConnected;
    private int type;  //0:Null 1:MOBILE 2:WIFI

    public NetWorkEvent(boolean isConnected, int type) {
        this.isConnected = isConnected;
        this.type = type;
    }

    public boolean isConnected() {
        return isConnected;
    }

    public void setConnected(boolean connected) {
        isConnected = connected;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
