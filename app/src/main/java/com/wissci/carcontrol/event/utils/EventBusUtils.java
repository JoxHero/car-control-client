package com.wissci.carcontrol.event.utils;

import org.greenrobot.eventbus.EventBus;

public class EventBusUtils {

    /**
     * 绑定 接受者
     *
     * @param subscriber
     */
    public static void register(Object subscriber) {
        EventBus.getDefault().register(subscriber);
    }

    /**
     * 解绑定
     *
     * @param subscriber
     */
    public static void unregister(Object subscriber) {
        EventBus.getDefault().unregister(subscriber);
    }

    /**
     * 是否绑定
     *
     * @param subscriber
     */
    public static boolean isRegister(Object subscriber) {
        return EventBus.getDefault().isRegistered(subscriber);
    }
}