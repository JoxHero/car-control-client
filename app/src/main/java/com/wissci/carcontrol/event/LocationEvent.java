package com.wissci.carcontrol.event;

import lombok.Data;

/**
 * Created by yongpengzhang on 2021/7/2
 */
@Data
public class LocationEvent {
    private String province;
    private String city;
    private String area;
    private String address;

    private boolean isLocationSuccess;
}
