package com.wissci.carcontrol;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationListener;
import com.vise.baseble.ViseBle;
import com.vise.baseble.callback.scan.IScanCallback;
import com.vise.baseble.callback.scan.SingleFilterScanCallback;
import com.vise.baseble.common.PropertyType;
import com.vise.baseble.model.BluetoothLeDevice;
import com.vise.baseble.model.BluetoothLeDeviceStore;
import com.vise.baseble.utils.BleUtil;
import com.vise.baseble.utils.HexUtil;
import com.vise.log.ViseLog;
import com.vise.log.inner.LogcatTree;
import com.wissci.carcontrol.base.mvp.BaseMvpActivity;
import com.wissci.carcontrol.bluetooth.BluetoothDeviceManager;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.utils.tool.ToastUtil;
import com.wissci.carcontrol.event.NetWorkChangeEvent;
import com.wissci.carcontrol.event.bluetooth.ConnectEvent;
import com.wissci.carcontrol.event.bluetooth.NotifyDataEvent;
import com.wissci.carcontrol.navigation.ShowDialogManager;
import com.wissci.carcontrol.output.Logger;
import com.wissci.carcontrol.service.JobService;
import com.wissci.carcontrol.service.LocationService;
import com.wissci.carcontrol.view.ui.bluetooth.DeviceControlActivity;
import com.wissci.carcontrol.view.ui.home.FirstLevelPopupWindow;
import com.wissci.carcontrol.view.ui.home.HomeFragment;
import com.wissci.carcontrol.view.ui.home.ScendLevelDialog;
import com.wissci.carcontrol.view.ui.setting.SettingFragment;
import com.wissci.carcontrol.view.widget.tablayout.SegmentTabLayout;
import com.wissci.carcontrol.view.widget.tablayout.listener.OnTabSelectListener;
import com.wissci.carcontrol.voice.VoiceUpRecognitionManager;

import org.apache.commons.collections4.CollectionUtils;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import butterknife.BindView;
import butterknife.OnClick;

import javax.inject.Inject;

import static androidx.fragment.app.FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT;
import static com.wissci.carcontrol.bluetooth.BluetoothDeviceManager.BLUETOOTH_NAME;

public class MainActivity extends BaseMvpActivity<MainPresenter> implements MainViewImpl {
    private static final int TO_WIFI_REQUEST_CODE = 1001;
    @BindView(R.id.tablayout)
    SegmentTabLayout tabLayout;
    @BindView(R.id.vp_conteent)
    ViewPager vpContent;
    @BindView(R.id.tv_bluetooth_state)
    TextView tvBlueToothState;
    @Inject
    PadManager padManager;
    private String[] tabTitles = {"首页", "设置"};
    private Intent mServiceIntent;
    private JobService jobService;

    private FirstLevelPopupWindow firstLevelPopupWindow;
    private ServiceConnection serviceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            jobService = null;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            JobService.JobBinder binder = (JobService.JobBinder) service;
            jobService = binder.getService();
            mainViewModel.setJobService(jobService);
        }
    };

    private MainViewModel mainViewModel;
    /**
     * 是否连接蓝牙
     */
    private boolean isBlueToothConnect;

    /**
     * 扫描回调
     */
    private SingleFilterScanCallback periodScanCallback = (SingleFilterScanCallback) new SingleFilterScanCallback(new IScanCallback() {
        @Override
        public void onDeviceFound(BluetoothLeDevice bluetoothLeDevice) {
            Logger.w("xwl Founded Scan Device:" + bluetoothLeDevice);
            if (!BluetoothDeviceManager.getInstance().isConnected(bluetoothLeDevice)) {
                BluetoothDeviceManager.getInstance().connect(bluetoothLeDevice);
                bluetoothDevice = bluetoothLeDevice;
                presenter.setBluetoohDevice(bluetoothLeDevice);
                //mainViewModel.setBluetoothDevice(bluetoothDevice);
                configBluetooth();
            }
            stopScan();
        }

        @Override
        public void onScanFinish(BluetoothLeDeviceStore bluetoothLeDeviceStore) {
            Logger.w("xwl scan finish " + bluetoothLeDeviceStore);
        }

        @Override
        public void onScanTimeout() {
            Logger.w("xwl scan timeout");
        }
    }).setDeviceName(BLUETOOTH_NAME);
    private ScendLevelDialog secondLevelDialog;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    public void setupView() {
        initTabViewPager();
    }

    private void initTabViewPager() {
        List<Fragment> fragments = Arrays.asList(HomeFragment.newInstance(),
                SettingFragment.newInstance());
        tabLayout.setTabData(tabTitles);
        tabLayout.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                vpContent.setCurrentItem(position);
            }

            @Override
            public void onTabReselect(int position) {

            }
        });

        FragmentPagerAdapter pagerAdapter = new FragmentPagerAdapter(getSupportFragmentManager(), BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
            @Override
            public int getCount() {
                return fragments.size();
            }

            @Override
            public Fragment getItem(int position) {
                return fragments.get(position);
            }
        };

        vpContent.setAdapter(pagerAdapter);
        vpContent.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout.setCurrentTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    @Override
    public void initialize() {
        ViseLog.getLogConfig().configAllowLog(true);//配置日志信息
        ViseLog.plant(new LogcatTree());//添加Logcat打印信息
        checkBluetoothPermission();
        mainViewModel = new ViewModelProvider(this).get(MainViewModel.class);
        mainViewModel.getSendMsg().observe(this, new Observer<byte[]>() {
            @Override
            public void onChanged(byte[] bytes) {
                send(bytes);
            }
        });
        presenter.setMainViewModel(mainViewModel);
        startService(new Intent(this,LocationService.class));
        VoiceUpRecognitionManager.getInstance(getApplicationContext()).init();
        firstLevelPopupWindow = new FirstLevelPopupWindow(this,padManager);
        //RxJava.timer(5, () -> VoiceUpRecognitionManager.getInstance(getApplicationContext()).init());
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPermission();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startScan();
    }

    /**
     * 开始扫描
     */
    private void startScan() {
        //AC_PAD_A106
//        ViseBle.getInstance().startScan(periodScanCallback);
//        ViseBle.getInstance().startScanByUuid(periodScanCallback);
        ViseBle.getInstance().startScanByDeviceName(periodScanCallback);
    }

    /**
     * 停止扫描
     */
    private void stopScan() {
        ViseBle.getInstance().stopScan(periodScanCallback);
    }

    private void configBluetooth() {
        UUID uuidService = UUID.fromString("6E400001-B5A3-F393-E0A9-E50E24DCCA9E");
        UUID uuidWrite = UUID.fromString("6E400002-B5A3-F393-E0A9-E50E24DCCA9E");
        UUID uuidRead = UUID.fromString("6E400003-B5A3-F393-E0A9-E50E24DCCA9E");
        RxJava.timer(3, () -> BluetoothDeviceManager.getInstance().bindChannel(bluetoothDevice, PropertyType.PROPERTY_WRITE, uuidService, uuidWrite, null));

        RxJava.timer(6, () -> {
            BluetoothDeviceManager.getInstance().bindChannel(bluetoothDevice, PropertyType.PROPERTY_READ, uuidService, uuidRead, null);
            BluetoothDeviceManager.getInstance().read(bluetoothDevice);
        });

        RxJava.timer(9, () -> {
            BluetoothDeviceManager.getInstance().bindChannel(bluetoothDevice, PropertyType.PROPERTY_NOTIFY, uuidService, uuidRead, null);
            BluetoothDeviceManager.getInstance().registerNotify(bluetoothDevice, false);
        });
        updateConnectedDevice();
    }

    public void send(byte[] data) {
        BluetoothDeviceManager.getInstance().write(bluetoothDevice, data);
    }

    @Override
    public void onStart() {
        super.onStart();
        // 去掉wifi通信方式
        /*netLogic();
        mServiceIntent = new Intent(this, JobService.class);
        bindService(mServiceIntent, serviceConnection, BIND_AUTO_CREATE);*/
    }

    /**
     * 检查蓝牙权限
     */
    private void checkBluetoothPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            //校验是否已具有模糊定位权限
            checkPermission(new CheckPermissionListener() {
                @Override
                public void hasPermission() {
                    enableBluetooth();
                }

                @Override
                public void noPermission() {
                    showNoPermissionDialog();
                }
            }, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        } else {
            enableBluetooth();
        }
    }

    private void enableBluetooth() {
        if (!BleUtil.isBleEnable(this)) {
            BleUtil.enableBluetooth(this, 1);
        } else {
            boolean isSupport = BleUtil.isSupportBle(this);
            boolean isOpenBle = BleUtil.isBleEnable(this);
            updateConnectedDevice();
        }
    }

    BluetoothLeDevice bluetoothDevice;

    /**
     * 更新已经连接到的设备
     */
    private void updateConnectedDevice() {
        if (ViseBle.getInstance().getDeviceMirrorPool() != null) {
            List<BluetoothLeDevice> bluetoothLeDeviceList = ViseBle.getInstance().getDeviceMirrorPool().getDeviceList();
            if (CollectionUtils.isNotEmpty(bluetoothLeDeviceList)) {
                //如果已经连接到指定名称的蓝牙设备，取到它
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    bluetoothLeDeviceList.stream().filter(
                            device -> device.getName().equals(BLUETOOTH_NAME)).findFirst()
                            .ifPresent(device -> {
                                bluetoothDevice = device;
                                presenter.setBluetoohDevice(bluetoothDevice);
                                //mainViewModel.setBluetoothDevice(bluetoothDevice);
                            });
                }
            }
        }
        isBlueToothConnect = bluetoothDevice != null;
        tvBlueToothState.setText(isBlueToothConnect ? "已连接" : "未连接");
    }

    @OnClick({R.id.tv_bluetooth_state})
    public void OnClick(View view) {
        if (view.getId() == R.id.tv_bluetooth_state) {
            if (isBlueToothConnect && bluetoothDevice != null) {
                //如果已经连接，跳转操作页面
                Intent intent = new Intent(this, DeviceControlActivity.class);
                intent.putExtra(DeviceControlActivity.EXTRA_DEVICE, bluetoothDevice);
                startActivity(intent);
            } else {
                //如果未连接则进行连接
                startScan();
                //BluetoothDeviceManager.getInstance().connect();
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void showConnectedDevice(ConnectEvent event) {
        if (event != null) {
            updateConnectedDevice();
            if (event.isDisconnected()) {
                ToastUtil.showToast(MainActivity.this, "Disconnect!");
            }
        }
    }

    private void netLogic() {
        //设备没网 或者 没有连接wifi热点 直接跳转到系统网络连接页面
        if (!Constants.NetWorkState.isOnline || Constants.NetWorkState.NETWORK_TYPE != Constants.NetWorkState.WIFI_TYPE) {
            startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
        } else {
            //如果连接的不是指定热点弹窗提示
            if (Constants.NetWorkState.WIFI_SSID != Constants.WIFI_NAME) {
                ShowDialogManager.showOnlyConfirmDialog(this, "", getString(R.string.NOT_CONNECTED_TO_A_DESIGNATED_HOTSPOT),
                        (dialog, which) -> {
                            startActivityForResult(new Intent(Settings.ACTION_WIFI_SETTINGS), TO_WIFI_REQUEST_CODE);
                            dialog.dismiss();
                        }, false);
            }
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleNetWorkChange(NetWorkChangeEvent event) {
        //当网络变化 当前连接的热点是指定热点重新连接socket
        if (Constants.NetWorkState.isOnline &&
                Constants.NetWorkState.NETWORK_TYPE == Constants.NetWorkState.WIFI_TYPE &&
                Constants.NetWorkState.WIFI_SSID == Constants.WIFI_NAME) {
            if (jobService != null) {
                jobService.connectSocket();
            }
        } else {
            ToastUtil.show(getString(R.string.disconnect));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(serviceConnection);
        Intent stopIntent = new Intent(MainActivity.this, LocationService.class);
        stopService(stopIntent);
    }

    long firstClick = 0;

    @Override
    public void onBackPressed() {
        if (System.currentTimeMillis() - firstClick > 2000) {
            ToastUtil.show(getContext(), getString(R.string.back_two_times_exit));
            firstClick = System.currentTimeMillis();
        } else {
            this.finish();
            System.exit(0);
        }
    }

    private final int permissionRequestCode = 100;
    private final List<String> mPermissionList = new ArrayList<>();
    private final String[] permissions = new String[]{
            //位置
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            //手机信息
            Manifest.permission.READ_PHONE_STATE,
            //内存
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
    };

    private void initPermission() {
        mPermissionList.clear();
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                mPermissionList.add(permission);
            }
        }
        if (mPermissionList.size() > 0) {
            ActivityCompat.requestPermissions(this, permissions, permissionRequestCode);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                           int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (permissionRequestCode == requestCode) {

        }
    }

    @Override public void showFirstLevel(PadManager padManager) {
        if (firstLevelPopupWindow.isShowing()){
            firstLevelPopupWindow.dismiss();
        }
        firstLevelPopupWindow.showAsDropDown(tabLayout,tabLayout.getWidth(),0);
    }

    @Override public void dissMissFirstLevel() {
        if (firstLevelPopupWindow != null){
            if (firstLevelPopupWindow.isShowing()){
                firstLevelPopupWindow.dismiss();
            }
        }
    }

    @Override public void showSecondLevel(PadManager padManager) {
        if (secondLevelDialog != null){
            if (secondLevelDialog.getDialog().isShowing()){
                return;
            }
        }
        secondLevelDialog = new ScendLevelDialog();
        secondLevelDialog.show(getSupportFragmentManager(),"");
    }

    @Override public void dissMissSecondLevel(PadManager padManager) {
        if (secondLevelDialog != null){
            secondLevelDialog.dismiss();
        }
    }
}
