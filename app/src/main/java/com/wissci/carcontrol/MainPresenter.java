package com.wissci.carcontrol;

import android.util.Log;
import com.vise.baseble.model.BluetoothLeDevice;
import com.vise.baseble.utils.HexUtil;
import com.wissci.carcontrol.base.presenter.BasePresenter;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.utils.tool.ToastUtil;
import com.wissci.carcontrol.event.SpeedEvent;
import com.wissci.carcontrol.event.bluetooth.NotifyDataEvent;
import com.wissci.carcontrol.utils.ByteConstants;
import com.wissci.carcontrol.utils.HexUtils;
import lombok.Getter;
import lombok.Setter;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import javax.inject.Inject;

public class MainPresenter extends BasePresenter<MainViewImpl> {
    @Setter
    private MainViewModel mainViewModel;
    private PadManager padManager;
    @Getter @Setter
    private BluetoothLeDevice bluetoohDevice;
    private float speed;

    @Inject
    public MainPresenter(PadManager padManager) {
        this.padManager = padManager;
        EventBus.getDefault().register(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void receiveData(NotifyDataEvent event) {
        if (event != null && event.getData() != null && event.getBluetoothLeDevice() != null
                && event.getBluetoothLeDevice().getAddress().equals(bluetoohDevice.getAddress())){
           byte[] receiveData = event.getData();
           parsReceiveData(receiveData);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void handleSpeed(SpeedEvent event) {
        speed =  event.getSpeed();
        boolean isShowSecondLevel = false;
        //触发二级报警
        if (speed > 5 && padManager.isMainPadHavePeople && !padManager.isMainSeatBelt){
            isShowSecondLevel = true;
            Log.d(Constants.APP_DATA_TRANS, "二级报警!");
        }

        if (speed > 10 && padManager.isVicePadHavePeople && !padManager.isViceSeatBelt){
            isShowSecondLevel = true;
            Log.d(Constants.APP_DATA_TRANS, "二级报警!");
        }

        if (isShowSecondLevel){
            view.showSecondLevel(padManager);
        }
    }

    /**
     * 解析收到的数据
     *
     * @param receiveData
     */
    private void parsReceiveData(byte[] receiveData) {
        Log.d(Constants.APP_DATA_TRANS, "平板收到数据 data : " + HexUtil.encodeHexStr(receiveData));

        //判断帧头、帧尾数据是否是约定数据
        if (ByteConstants.FRAME_HEADER != receiveData[0] && ByteConstants.FRAME_END != receiveData[receiveData.length - 1]){
            return;
        }
        byte target = receiveData[2];

        //如果目标地址不是平板说明是反馈数据不做处理
        if (target != ByteConstants.DEVICE_PAD){
            Log.d(Constants.APP_DATA_TRANS, "平板收到反馈数据");
            return;
        }

        //功能号
        byte functionNum = receiveData[3];
        //压力传感器数据
        byte pressureSensorData = receiveData[4];
        //安全带数据
        byte seatBeltData = receiveData[5];

        if (ByteConstants.FUNCTION_SEAT_PERSONNEL == functionNum){
            //座椅人员信息数据
            // TODO: 2021/8/13 暂时只处理两个座椅
            Log.d(Constants.APP_DATA_TRANS, "=================================================");
            Log.d(Constants.APP_DATA_TRANS, "平板收到座椅信息数据 data : " + HexUtils.hexadecimalToBinary(pressureSensorData));
            char[] p = HexUtils.hexadecimalToBinary(pressureSensorData).toCharArray();
            Log.d(Constants.APP_DATA_TRANS, "主驾有人 ？" + (p[0] == '1'));
            padManager.setMainPadHavePeople(p[0] == '1');
            if (p.length > 1){
                Log.d(Constants.APP_DATA_TRANS, "副驾有人 ？" + (p[1] == '1'));
                padManager.setVicePadHavePeople(p[1] == '1');
            }
            char[] s = HexUtils.hexadecimalToBinary(seatBeltData).toCharArray();
            Log.d(Constants.APP_DATA_TRANS, "安全带数据 data : " + HexUtils.hexadecimalToBinary(seatBeltData));
            Log.d(Constants.APP_DATA_TRANS, "主驾系安全带 ？" + (s[0] == '1'));
            padManager.setMainSeatBelt(s[0] == '1');
            if (s.length > 1){
                Log.d(Constants.APP_DATA_TRANS, "副驾系安全带 ？" + (s[1] == '1'));
                padManager.setViceSeatBelt(s[1] == '1');
            }
            //发送反馈信息，目标地址改为主控板
            receiveData[2] = ByteConstants.DEVICE_MAIN_CONTROL;
            mainViewModel.sendMsg(receiveData);

            //发送座椅人员信息改变事件,处理相应逻辑
            //EventBus.getDefault().post(new SeatPersonnelChangeEvent(receiveData));

            //触发一级报警
            boolean isShowFirstLevel = false;
            //主驾有人没有系安全带 提示
            if ((p[0] == '1') && !(s[0] == '1')){
                isShowFirstLevel = true;
                Log.d(Constants.APP_DATA_TRANS, "主驾一级报警!");
                //ToastUtil.show(view.getContext(),view.getContext().getString(R.string.please_main_seat_belt));
            }

            //副驾有人没有系安全带 提示
            if ((p[1] == '1') && !(s[1] == '1')){
                isShowFirstLevel = true;
                Log.d(Constants.APP_DATA_TRANS, "副驾一级报警!");
                //ToastUtil.show(view.getContext(),view.getContext().getString(R.string.please_vice_seat_belt));
            }

            if (isShowFirstLevel){
                view.showFirstLevel(padManager);
            }else{
                view.dissMissFirstLevel();
                view.dissMissSecondLevel(padManager);
            }

            Log.d(Constants.APP_DATA_TRANS, "=================================================");
        }

        //主控板获取车速,获取一次发一次
        if (ByteConstants.FUNCTION_GET_SPEED == functionNum){
            Log.d(Constants.APP_DATA_TRANS, "平板收到获取车速指令");
            sendSpeedToMain();
        }
    }

    /**
     * 发送速度信息到主控板
     */
    public void sendSpeedToMain(){
        byte[] sendMainControlBytes = new byte[] {
                ByteConstants.FRAME_HEADER,
                ByteConstants.FRAME_HEADER2,
                ByteConstants.DEVICE_MAIN_CONTROL,
                ByteConstants.FUNCTION_SEND_SPEED,
                0,
                0,
                0,
                0,
                ByteConstants.TEMP_DATA,
                ByteConstants.TEMP_DATA,
                ByteConstants.FRAME_END,
                ByteConstants.FRAME_END2};

        String speedStr = Integer.toHexString(Float.floatToIntBits(padManager.getSpeed() * 3.6f));
        byte[] speedBytes = HexUtil.decodeHex(speedStr);
        if (speedBytes.length == 4){
            sendMainControlBytes[4] = speedBytes[0];
            sendMainControlBytes[5] = speedBytes[1];
            sendMainControlBytes[6] = speedBytes[2];
            sendMainControlBytes[7] = speedBytes[3];
            mainViewModel.sendMsg(sendMainControlBytes);
        }
    }

    @Override public void destroy() {
        super.destroy();
        if (EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }
}
