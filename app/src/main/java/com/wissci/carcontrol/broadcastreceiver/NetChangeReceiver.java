package com.wissci.carcontrol.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.utils.tool.NetWorkUtils;

/**
 * @Author: yongpeng.zhang@ixiandou.com.
 * @Time: 2019-09-19
 */
public class NetChangeReceiver extends BroadcastReceiver {
    private static final String TAG = "NetChangeReceiver";
    private NetWorkChange netWorkChange;

    public interface NetWorkChange {
        void netWorkConnected();

        void netWorkChange();

        void netWorkDisconnected();
    }

    public NetChangeReceiver() {
    }

    public NetChangeReceiver(NetWorkChange netWorkChange) {
        this.netWorkChange = netWorkChange;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "receive network status change ...");
        if (!NetWorkUtils.isOnline(context)) {
            Constants.NetWorkState.NETWORK_TYPE = Constants.NetWorkState.NONE_TYPE;
            Constants.NetWorkState.isOnline = false;
            netWorkChange.netWorkDisconnected();
            Log.d(TAG, "not online, so ignore.");
            return;
        }
        Constants.NetWorkState.isOnline = true;

        if (intent.getAction().equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)){
            NetWorkUtils.getNetWorkInfo(context);
            netWorkChange.netWorkChange();
            return;
        }

        /*NetWorkUtils.getNetWorkInfo(context);
        netWorkChange.netWorkConnected();*/
    }


}
