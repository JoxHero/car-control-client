package com.wissci.carcontrol.broadcastreceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SmsBroadcastReceiver extends BroadcastReceiver {
 
    private static final String TAG = "SmsBroadcastReceiver";
 
    private static MessageListener mMessageListener;
 
    public SmsBroadcastReceiver() {
        super();
        Log.i(TAG, "onReceive: super:" );
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive: 接收短信:");
        Toast.makeText(context,"接收短信",Toast.LENGTH_SHORT).show();
        Object[] pdus = (Object[]) intent.getExtras().get("pdus");
        for (Object pdu : pdus) {
            SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdu);
            String sender = smsMessage.getDisplayOriginatingAddress();
            String content = smsMessage.getMessageBody();

            if (!content.contains("闲豆回收")){
                return;
            }

            long date = smsMessage.getTimestampMillis();
            Date timeDate = new Date(date);
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String time = simpleDateFormat.format(timeDate);
            String code = getDynamicPwd(content);
            Log.i(TAG, "onReceive: 短信来自:" + sender);
            Log.i(TAG, "onReceive: 短信内容:" + content);
            Log.i(TAG, "onReceive: 短信时间:" + time);
            Log.i(TAG, "onReceive: 验证码:" + code);
            if (mMessageListener != null){
                mMessageListener.OnReceived(code);
            }
        }
    }

    public String getDynamicPwd(String content) {

        // 此正则表达式验证六位数字的短信验证码

        //Pattern pattern = Pattern.compile("(?<![0-9])([0-9]{" + 6 + "})(?![0-9])");
        Pattern pattern = Pattern.compile("(\\d{4})");

        Matcher matcher = pattern.matcher(content);

        String dynamicPwd = "";

        while (matcher.find()) {

            dynamicPwd = matcher.group();

            Log.i("TAG", "getDynamicPwd: find pwd=" + dynamicPwd);
        }
        return dynamicPwd;
    }
 
    // 回调接口
    public interface MessageListener {
 
        /**
         * 接收到自己的验证码时回调
         * @param message 短信内容
         */
        void OnReceived(String message);
    }
 
    /**
     * 设置验证码接收监听
     * @param messageListener 自己验证码的接受监听，接收到自己验证码时回调
     */
    public void setOnReceivedMessageListener(MessageListener messageListener) {
        mMessageListener = messageListener;
    }
}