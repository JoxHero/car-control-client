/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wissci.carcontrol.di.model;


import com.wissci.carcontrol.MainActivity;

import com.wissci.carcontrol.view.ui.bluetooth.DeviceControlActivity;
import com.wissci.carcontrol.view.ui.bluetooth.DeviceDetailActivity;
import com.wissci.carcontrol.view.ui.bluetooth.DeviceScanActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = MainFragmentBuildersModule.class)
    abstract MainActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = MainFragmentBuildersModule.class)
    abstract DeviceControlActivity contributeDeviceControlActivity();

    @ContributesAndroidInjector(modules = MainFragmentBuildersModule.class)
    abstract DeviceScanActivity contributeDeviceScanActivity();

    @ContributesAndroidInjector(modules = MainFragmentBuildersModule.class)
    abstract DeviceDetailActivity contributeDeviceDetailActivity();

}
