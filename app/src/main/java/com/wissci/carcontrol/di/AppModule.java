package com.wissci.carcontrol.di;

import android.app.Application;
import android.content.Context;

import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.common.utils.tool.SharedPreferencesUtils;

import com.wissci.carcontrol.navigation.NavigationController;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
public abstract class AppModule {

    @Provides
    @Singleton
    static Context provideApplicationContext(Application application) {
        return application.getApplicationContext();
    }

    @Provides
    @Singleton
    static NavigationController provideNavigator( SharedPreferencesUtils sharedPreferencesUtils) {
        return new NavigationController(sharedPreferencesUtils);
    }


    @Provides
    @Singleton
    static SharedPreferencesUtils provideSharedPreferencesUtils(Context applicationContext) {
        return SharedPreferencesUtils.getInstance(applicationContext);
    }

    @Provides
    @Singleton
    static PadManager providePadManager(Context applicationContext) {
        return new PadManager(applicationContext);
    }
}
