package com.wissci.carcontrol.di.model;


import com.wissci.carcontrol.service.JobService;
import com.wissci.carcontrol.service.LocationService;
import com.wissci.carcontrol.service.PhoneStateService;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by yongpengzhang on 2021/7/2
 */
@Module
public abstract class ServiceModule {
    @ContributesAndroidInjector
    abstract LocationService contributeLocationService();
    @ContributesAndroidInjector
    abstract PhoneStateService contributePhoneStateService();
    @ContributesAndroidInjector
    abstract JobService contributeJobService();
}
