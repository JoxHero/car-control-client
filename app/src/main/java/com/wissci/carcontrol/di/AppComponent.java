package com.wissci.carcontrol.di;

import android.app.Application;

import com.wissci.carcontrol.AppApplication;
import com.wissci.carcontrol.di.model.ActivityModule;
import com.wissci.carcontrol.di.model.ServiceModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjectionModule;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton
@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                AndroidInjectionModule.class,
                AppModule.class,
                ActivityModule.class,
                ServiceModule.class
        })
public interface AppComponent extends AndroidInjector<AppApplication> {
    @Override
    void inject(AppApplication app);


    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);
        AppComponent build();
    }

}
