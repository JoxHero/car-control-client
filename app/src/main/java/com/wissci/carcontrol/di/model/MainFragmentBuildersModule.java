/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.wissci.carcontrol.di.model;

import com.wissci.carcontrol.view.ui.home.HomeFragment;
import com.wissci.carcontrol.view.ui.location.LocationFragment;
import com.wissci.carcontrol.view.ui.setting.SettingFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * @Description:Fragment注入
 * Created by yongpengzhang on 2021/7/2
 */
@Module
public abstract class MainFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract HomeFragment HomeFragmentInjector();

    @ContributesAndroidInjector
    abstract LocationFragment LocationFragmentInjector();

    @ContributesAndroidInjector
    abstract SettingFragment SettingFragmentInjector();

}
