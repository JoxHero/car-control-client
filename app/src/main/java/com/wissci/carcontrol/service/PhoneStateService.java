package com.wissci.carcontrol.service;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;

import com.wissci.carcontrol.broadcastreceiver.NetChangeReceiver;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.event.NetWorkChangeEvent;
import com.wissci.carcontrol.event.NetWorkEvent;

import org.greenrobot.eventbus.EventBus;


/**
 * @Description: 监听网络状态的服务
 * @Author: yongpeng.zhang@ixiandou.com.
 * @Time: 2019-09-19
 */
public class PhoneStateService extends JobIntentService {

    private static final String TAG = "PhoneStateService";
    public static final int JOB_ID = 1;
    private NetChangeReceiver receiver;
    private NetChangeReceiver.NetWorkChange netWorkChange;
    private EventBus rxBus;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, PhoneStateService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        rxBus = EventBus.getDefault();
        netWorkChange = new NetChangeReceiver.NetWorkChange() {
            @Override
            public void netWorkConnected() {
                Log.d(TAG, "netWorkConnected: network is connect!");
                rxBus.post(new NetWorkEvent(true, Constants.NetWorkState.NETWORK_TYPE));
            }

            @Override
            public void netWorkDisconnected() {
                Log.d(TAG, "netWorkConnected: network is disconnect!!!");
                rxBus.post(new NetWorkEvent(false, Constants.NetWorkState.NONE_TYPE));
            }

            @Override public void netWorkChange() {
                rxBus.post(new NetWorkChangeEvent(true, Constants.NetWorkState.NETWORK_TYPE));
            }
        };
        receiver = new NetChangeReceiver(netWorkChange);
        registerReceiver(receiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (receiver != null) {
            unregisterReceiver(receiver);
        }
    }
}
