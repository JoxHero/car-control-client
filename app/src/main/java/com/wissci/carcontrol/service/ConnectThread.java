package com.wissci.carcontrol.service;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.utils.ByteConstants;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import javax.inject.Inject;

import static com.wissci.carcontrol.common.Constants.Status.DEVICE_CONNECTED;
import static com.wissci.carcontrol.common.Constants.Status.GET_MSG;
import static com.wissci.carcontrol.common.Constants.Status.SEND_MSG_ERROR;
import static com.wissci.carcontrol.common.Constants.Status.SEND_MSG_SUCCSEE;

/**
 * @Description:连接线程 Created by yongpengzhang on 2021/8/1
 */
public class ConnectThread extends Thread {
  private final Socket socket;
  private Handler handler;
  private InputStream inputStream;
  private OutputStream outputStream;

  @Inject PadManager padManager;

  public ConnectThread(Socket socket, Handler handler) {
    setName("ConnectThread");
    Log.i("ConnectThread", "ConnectThread");
    this.socket = socket;
    this.handler = handler;
  }

  @Override public void run() {
    if (socket == null) {
      return;
    }
    handler.sendEmptyMessage(DEVICE_CONNECTED);
    try {
      //获取数据流
      inputStream = socket.getInputStream();
      outputStream = socket.getOutputStream();

      byte[] buffer = new byte[1024 * 4];
      int bytes;
      while (true) {
        //读取数据
        bytes = inputStream.read(buffer);
        if (bytes > 0) {
          final byte[] data = new byte[bytes];
          System.arraycopy(buffer, 0, data, 0, bytes);

          parsReceiveData(data);

          Message message = Message.obtain();
          message.what = GET_MSG;
          Bundle bundle = new Bundle();
          bundle.putString("MSG", new String(data));
          message.setData(bundle);
          handler.sendMessage(message);

          Log.i("ConnectThread", "读取到数据:" + new String(data));
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * 解析收到的数据
   *
   * @param receiveData
   */
  private void parsReceiveData(byte[] receiveData) {
    if (receiveData.length != 7) {
      return;
    }

    //判断帧头、帧尾数据是否是约定数据
    if (ByteConstants.FRAME_HEADER != receiveData[0] && ByteConstants.FRAME_END != receiveData[6]){
      return;
    }

    //功能号
    byte functionNum = receiveData[2];
    //压力传感器数据
    byte pressureSensorData = receiveData[3];
    //安全带数据
    byte seatBeltData = receiveData[4];

    switch (functionNum) {
      case ByteConstants.FUNCTION_SEAT_PERSONNEL:
        //座椅人员信息数据
        // TODO: 2021/8/13 暂时只处理两个座椅
        char[] p = hexadecimalToBinary(pressureSensorData).toCharArray();
        char[] s = hexadecimalToBinary(seatBeltData).toCharArray();
        padManager.isMainPadHavePeople = (p[0] == 1);
        padManager.isVicePadHavePeople = (p[1] == 1);
        padManager.isMainSeatBelt = (s[0] == 1);
        padManager.isViceSeatBelt = (s[1] == 1);
        break;
      case ByteConstants.FUNCTION_MASSAGE:
        //按摩数据
        break;
      case ByteConstants.FUNCTION_DIRECTION_MOTOR:
        //向电机调节数据
        break;
      case ByteConstants.FUNCTION_VENTILATION_HEAT:
        //通风加热数据
        break;
      case ByteConstants.FUNCTION_RGB_LIGHT_STRIP:
        //RGB灯带数据
        break;
      default:
        break;
    }
  }

  /**
   * 16进制转2进制
   * @param data
   * @return
   */
  public String hexadecimalToBinary(byte data){
    int ten = Integer.parseInt(String.valueOf(data), 16);
    String two = Integer.toBinaryString(ten);
    return two;
  }

  /**
   * 发送数据
   */
  public void sendData(byte[] bytes) {
    Log.i("ConnectThread", "发送数据:" + (outputStream == null));
    if (outputStream != null) {
      try {
        outputStream.write(bytes);
        outputStream.flush();
        Log.i("ConnectThread", "发送消息：" + bytes);
        Message message = Message.obtain();
        message.what = SEND_MSG_SUCCSEE;
        Bundle bundle = new Bundle();
        bundle.putString("MSG", new String(bytes));
        message.setData(bundle);
        handler.sendMessage(message);
      } catch (IOException e) {
        e.printStackTrace();
        Message message = Message.obtain();
        message.what = SEND_MSG_ERROR;
        Bundle bundle = new Bundle();
        bundle.putString("MSG", new String(bytes));
        message.setData(bundle);
        handler.sendMessage(message);
      }
    }
  }

  public void close() {
    this.close();
    try {
      if (socket != null) socket.close();
      if (inputStream != null) inputStream.close();
      if (outputStream != null) outputStream.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
