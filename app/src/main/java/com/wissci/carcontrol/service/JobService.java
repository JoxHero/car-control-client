package com.wissci.carcontrol.service;

import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;

import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.event.StatusEvent;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.net.Socket;

import dagger.android.DaggerService;

import static com.wissci.carcontrol.common.Constants.Status.DEVICE_CONNECTED;
import static com.wissci.carcontrol.common.Constants.Status.DEVICE_CONNECTING;
import static com.wissci.carcontrol.common.Constants.Status.DEVICE_CONNECT_FAIL;
import static com.wissci.carcontrol.common.Constants.Status.GET_MSG;
import static com.wissci.carcontrol.common.Constants.Status.SEND_MSG_ERROR;
import static com.wissci.carcontrol.common.Constants.Status.SEND_MSG_SUCCSEE;

/**
 * @Description: 工作service
 * Created by yongpengzhang on 2021/8/1
 */
public class JobService extends DaggerService {
    public static final String TAG = "JobService";
    private final IBinder binder = new JobBinder();
    /**
     * 连接线程
     */
    private ConnectThread connectThread;
    /**
     * 监听线程
     */
    private ListenerThread listenerThread;

    private final Handler statusHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            int state = msg.what;
            String message = msg.getData().getString("MSG");
            switch (msg.what) {
                case DEVICE_CONNECTING:
                    Log.d(TAG, "设备连接中～");
                    connectThread = new ConnectThread(listenerThread.getSocket(), statusHandler);
                    connectThread.start();
                    break;
                case DEVICE_CONNECTED:
                    Log.d(TAG, "设备连接成功" + message);
                    break;
                case SEND_MSG_SUCCSEE:
                    Log.d(TAG, "发送消息成功" + message);
                    break;
                case SEND_MSG_ERROR:
                    Log.d(TAG, "发送消息失败" + message);
                    break;
                case GET_MSG:
                    Log.d(TAG, "收到消息" + message);
                    break;
                default:
                    break;
            }
            EventBus.getDefault().post(new StatusEvent(state, message));
            return false;
        }
    });

    public JobService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        connectSocket();
    }

    public void connectSocket() {
        new Thread(() -> {
            try {
                if (!Constants.NetWorkState.isOnline) {
                    return;
                }
                Socket socket = new Socket(Constants.HOST, Constants.PORT);
                connectThread = new ConnectThread(socket, statusHandler);
                connectThread.start();
            } catch (IOException e) {
                e.printStackTrace();
                statusHandler.sendEmptyMessage(DEVICE_CONNECT_FAIL);
            }
        }).start();

        listenerThread = new ListenerThread(Constants.PORT, statusHandler);
        listenerThread.start();
    }

    /**
     * 向加热垫发送指令
     *
     * @param bytes 数据包
     */
    public void sendMsgToPad(byte[] bytes) {
        if (connectThread == null) {
            return;
        }
        connectThread.sendData(bytes);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (connectThread != null) {
            connectThread.close();
        }

        if (listenerThread != null) {
            listenerThread.close();
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    public class JobBinder extends Binder {
        public JobService getService() {
            return JobService.this;
        }
    }
}