package com.wissci.carcontrol.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import androidx.annotation.Nullable;
import com.amap.api.location.AMapLocation;
import com.amap.api.location.AMapLocationClient;
import com.amap.api.location.AMapLocationClientOption;
import com.amap.api.location.AMapLocationListener;
import com.wissci.carcontrol.PadManager;
import com.wissci.carcontrol.common.Constants;
import com.wissci.carcontrol.common.utils.tool.SharedPreferencesUtils;
import com.wissci.carcontrol.common.utils.tool.ToastUtil;
import com.wissci.carcontrol.event.LocationEvent;
import com.wissci.carcontrol.event.SpeedEvent;
import dagger.android.DaggerService;
import org.greenrobot.eventbus.EventBus;

import javax.inject.Inject;

public class LocationService extends DaggerService implements  AMapLocationListener {
    @Inject
    SharedPreferencesUtils sharedPreferencesUtils;
    @Inject
    PadManager padManager;
    Context context;
    private AMapLocationClient locationClient;
    private AMapLocationClientOption locationOption;

    /**
     * 开始定位
     */
    public final static int MSG_LOCATION_START = 0;
    /**
     * 定位完成
     */
    public final static int MSG_LOCATION_FINISH = 1;
    /**
     * 停止定位
     */
    public final static int MSG_LOCATION_STOP = 2;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        activate();
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);

    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    private void activate() {
        Log.d(Constants.APP_TAG, "定位开始+++++++");
        if (locationClient == null) {
            locationClient = new AMapLocationClient(this);
        }
        if (locationOption == null) {
            locationOption = new AMapLocationClientOption();
            //设置定位监听
            locationClient.setLocationListener(this);
            //设置为高精度定位模式
            locationOption.setLocationMode(AMapLocationClientOption.AMapLocationMode.Hight_Accuracy);

            locationOption.setHttpTimeOut(20000);
            //设置时间间隔
            locationOption.setInterval(5000);
            //返回详细地址
            locationOption.setGpsFirst(true);
            locationOption.setNeedAddress(false);
            locationOption.setSensorEnable(true);
        }
        //设置定位参数
        locationClient.setLocationOption(locationOption);
        locationClient.startLocation();
        mHandler.sendEmptyMessage(MSG_LOCATION_START);
    }

    @Override
    public void onLocationChanged(AMapLocation loc) {
        if (null != loc) {
            Message msg = mHandler.obtainMessage();
            msg.obj = loc;
            msg.what = MSG_LOCATION_FINISH;
            mHandler.sendMessage(msg);
        }
        //locationClient.stopLocation();
    }

    @SuppressLint("HandlerLeak")
    Handler mHandler = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            switch (msg.what) {
                //开始定位
                case MSG_LOCATION_START:
                    Log.d(Constants.APP_TAG, "开始定位：");
                    break;
                // 定位完成
                case MSG_LOCATION_FINISH:
                    AMapLocation loc = (AMapLocation) msg.obj;
                    String result = getLocationStr(loc);
                    padManager.setSpeed(loc.getSpeed());
                    double lat = loc.getLatitude();
                    double lng = loc.getLongitude();

                    EventBus.getDefault().post(new SpeedEvent(loc.getSpeed()));
                    Log.d(Constants.APP_TAG, "定位成功：" + lat + "+++++" + lng + "======" + loc.getAddress() + "-- " + result);
                    break;
                case MSG_LOCATION_STOP:
                    Log.d(Constants.APP_TAG, "停止定位");
                    break;
                default:
                    break;
            }
        }
    };


    /**
     * 根据定位结果返回定位信息的字符串
     *
     * @param
     * @return
     */
    public synchronized String getLocationStr(AMapLocation location) {
        if (null == location) {
            return null;
        }

        LocationEvent locationEvent = new LocationEvent();
        StringBuffer sb = new StringBuffer();
        //errCode等于0代表定位成功，其他的为定位失败，具体的可以参照官网定位错误码说明
        if (location.getErrorCode() == 0) {

            sb.append("定位成功" + "\n");
            sb.append("定位类型: " + location.getLocationType() + "\n");
            sb.append("经    度    : " + location.getLongitude() + "\n");
            sb.append("纬    度    : " + location.getLatitude() + "\n");
            sb.append("精    度    : " + location.getAccuracy() + "米" + "\n");
            sb.append("提供者    : " + location.getProvider() + "\n");

            if (location.getProvider().equalsIgnoreCase(
                    android.location.LocationManager.GPS_PROVIDER)) {
                // 以下信息只有提供者是GPS时才会有
                //ToastUtil.show(getApplicationContext(),location.getSpeed()+"米/秒");
                sb.append("速    度    : " + location.getSpeed() + "米/秒" + "\n");
                sb.append("角    度    : " + location.getBearing() + "\n");
                // 获取当前提供定位服务的卫星个数
                sb.append("星    数    : "
                        + location.getSatellites() + "\n");
            } else {
                // 提供者是GPS时是没有以下信息的
                sb.append("国    家    : " + location.getCountry() + "\n");
                sb.append("省            : " + location.getProvince() + "\n");
                sb.append("市            : " + location.getCity() + "\n");
                sb.append("城市编码 : " + location.getCityCode() + "\n");
                sb.append("区            : " + location.getDistrict() + "\n");
                sb.append("区域 码   : " + location.getAdCode() + "\n");
                sb.append("地    址    : " + location.getAddress() + "\n");
                sb.append("兴趣点    : " + location.getPoiName() + "\n");

                locationEvent.setProvince(location.getProvince());
                locationEvent.setCity(location.getCity());
                locationEvent.setArea(location.getDistrict());
                locationEvent.setLocationSuccess(true);
            }
        } else {
            //定位失败
            sb.append("定位失败" + "\n");
            sb.append("错误码:" + location.getErrorCode() + "\n");
            sb.append("错误信息:" + location.getErrorInfo() + "\n");
            sb.append("错误描述:" + location.getLocationDetail() + "\n");
            locationEvent.setLocationSuccess(false);
        }

        EventBus.getDefault().post(locationEvent);

        return sb.toString();
    }

    @Override
    public void onDestroy() {
        if (locationClient != null) {
            locationClient.onDestroy();
        }
        locationClient = null;
        super.onDestroy();
    }
}
