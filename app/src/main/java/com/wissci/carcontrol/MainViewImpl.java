package com.wissci.carcontrol;


import com.wissci.carcontrol.base.presenter.LoadDataView;

/**
 * @Author:  zyp
 * @Time:  2019-06-12 10:11
 */
public interface MainViewImpl extends LoadDataView {

  void showFirstLevel(PadManager padManager);

  void dissMissFirstLevel();

  void showSecondLevel(PadManager padManager);

  void dissMissSecondLevel(PadManager padManager);
}
