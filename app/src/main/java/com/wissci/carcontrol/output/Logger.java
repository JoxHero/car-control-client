package com.wissci.carcontrol.output;

import android.util.Log;

import org.json.JSONObject;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Locale;

/**
 * author : XieWenLong
 * e-mail : wenlong.xie@opay-inc.com
 * date   : 2021/8/28 10:56
 */
public class Logger {
    private static final String LOG_PREFIX = "bankcard_";
    private static final int LOG_PREFIX_LENGTH = LOG_PREFIX.length();
    private static final int MAX_LOG_TAG_LENGTH = 23;
    private static final String TAG = "OpayLog";

    public static final boolean DEBUG = true;

    public static String makeLogTag(String str) {
        if (str.length() > MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH) {
            return LOG_PREFIX + str.substring(0, MAX_LOG_TAG_LENGTH - LOG_PREFIX_LENGTH - 1);
        }

        return LOG_PREFIX + str;
    }

    private static StackTraceElement getCallerStackTraceElement() {
        return Thread.currentThread().getStackTrace()[4];
    }

    private static String makeLogTag(StackTraceElement caller) {
        if (caller == null) {
            return makeLogTag("service");
        }
        String className = caller.getClassName();
        className = className.substring(className.lastIndexOf(".") + 1);
        return String.format(Locale.CHINA, "%s.%s(L:%d)",
                className,
                caller.getMethodName(),
                caller.getLineNumber());
    }

    /**
     * debug信息
     *
     * @param msg
     */
    public static void debug(String msg) {
        if (DEBUG) {
            String tag = makeLogTag(getCallerStackTraceElement());
            Log.v(TAG, msg);
        }
    }

    /**
     * debug信息
     *
     * @param msg
     */
    public static void d(String msg) {
        if (DEBUG) {
            String tag = makeLogTag(getCallerStackTraceElement());
            Log.v(TAG, msg);
        }
    }

    /**
     * warning信息
     *
     * @param msg
     */
    public static void w(String msg) {
        if (DEBUG) {
            String tag = makeLogTag(getCallerStackTraceElement());
            Log.w(TAG, msg);
        }
    }

    /**
     * error信息
     *
     * @param msg
     */
    public static void e(String msg) {
        if (DEBUG) {
            String tag = makeLogTag(getCallerStackTraceElement());
            Log.e(TAG, msg);
        }
    }

    public static String toString(Object object) {
        try {
            JSONObject jsonObject = new JSONObject();
            StringBuilder buffer = new StringBuilder();
            Class<?> cls = object.getClass();
            buffer.append(cls.getSimpleName()).append(":");
            while (cls != null && cls != Object.class) {
                Logger.getField(object, cls, jsonObject);
                cls = cls.getSuperclass();
            }
            buffer.append(jsonObject.toString());
            return buffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private static void getField(Object object, Class<?> cls, JSONObject jsonObject) {
        try {
            Field[] fields = cls.getDeclaredFields();
            for (Field field : fields) {
                int mod = field.getModifiers();
                if (Modifier.isStatic(mod) || Modifier.isFinal(mod) || Modifier.isTransient(mod)) {
                    continue;
                }
                field.setAccessible(true);
                jsonObject.put(field.getName(), field.get(object));
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

}