package com.wissci.carcontrol;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

/**
 * author : XieWenLong
 * e-mail : wenlong.xie@opay-inc.com
 * date   : 2021/8/28 16:50
 */
public class RxJava {

    // Rxjava 主线程执行
    public static Disposable postOnMainThread(Action onComplete) {
        return Observable.empty().observeOn(AndroidSchedulers.mainThread())
                .subscribe(onNext -> {
                }, onError -> {
                }, onComplete);
    }

    // 定时任务,单位(秒)
    public static Disposable timer(long delay, Action onComplete) {
        return Observable.timer(delay, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(onNext -> {
                }, onError -> {
                }, onComplete);
    }

    // 周期任务,单位(毫秒)
    public static Disposable interval(long delay, Consumer<Long> consumer) {
        return Observable.interval(delay, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(consumer);
    }

    // 周期任务,单位(秒)
    public static Disposable intervalOnWorkThread(long delay, Consumer<Long> consumer) {
        return Observable.interval(delay, TimeUnit.SECONDS)
                .subscribeOn(Schedulers.io())
                .subscribe(consumer);
    }

    public static Disposable postOnWorkerThread(Action onComplete) {
        return Observable.empty().observeOn(Schedulers.io())
                .subscribe(onNext -> {
                }, onError -> {
                }, onComplete);
    }

    public static Disposable postOnWorkerThread(Action source, Action error) {
        return Single.create(emitter -> source.run())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(o -> {
                }, throwable -> error.run());
    }

    public static void dispose(Disposable disposable) {
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}